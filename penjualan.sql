-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2023 at 02:51 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `total_price` bigint(20) NOT NULL,
  `payment_received` int(11) NOT NULL DEFAULT 0,
  `payment_changes` int(11) NOT NULL DEFAULT 0,
  `customer_name` varchar(100) NOT NULL DEFAULT 'NN',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id`, `id_karyawan`, `total_price`, `payment_received`, `payment_changes`, `customer_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 9500000, 0, 0, 'NN', '2023-05-20 09:11:28', '2023-05-20 09:11:28', '2023-05-20 09:11:28'),
(4, 3, 43500000, 0, 0, 'NN', '2023-05-20 09:24:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 3, 9500000, 0, 0, 'NN', '2023-05-22 14:06:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 3, 9500000, 0, 0, 'NN', '2023-05-27 03:44:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 3, 9500000, 0, 0, 'Eko', '2023-05-27 03:48:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 3, 2397000, 0, 0, 'John Cena', '2023-06-02 15:04:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 3, 9500000, 10000000, 500000, 'Pitong', '2023-06-28 14:50:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
