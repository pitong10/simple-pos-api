<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Supplier extends REST_Controller {

    protected $table = "supplier";

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->database();
    }

     //Menampilkan data supplier
     function index_get() {
        $id = $this->get('id');
        if ($id != '') {
            $this->db->where('id', $id);
        }
        
        $data = $this->db->get($this->table)->result();

        if($data == null || $data == "") {
            $response = array(
                'data'      => [],
                'message'   => "Data tidak ditemukan",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $message = "Data ditemukan";
        $code = 200;
        $status = true;

        $response = array(
            'data'      => $data,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function lookup_get() {
        $query = $this->get('query');
        if ($query != '') {
            $this->db->like('name', $query);
        }
        
        $data = $this->db->get($this->table)->result();

        if($data == null || $data == "") {
            $response = array(
                'data'      => [],
                'message'   => "Data tidak ditemukan",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $message = "Data ditemukan";
        $code = 200;
        $status = true;

        $response = array(
            'data'      => $data,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function index_post() {

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('address', 'address', 'trim|required');
        $this->form_validation->set_rules('description', 'description', 'trim|required');

        if(!$this->form_validation->run()) {
            $response = array(
                'data'      => null,
                'message'   => strip_tags(validation_errors()),
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $data = array(
                    'name'                  => $this->post('name'),
                    'code'                  => $this->post('code'),
                    'phone'                 => $this->post('phone'),
                    'address'               => $this->post('address'),
                    'description'           => $this->post('description'),
                );

        // Get supplier availability
        $this->db->where('code', $data['code']);
        $category = $this->db->get($this->table)->result();
        if(sizeof($category) >= 1) {
            $response = array(
                'data'      => null,
                'message'   => "Supplier with same code already registered",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $insert = $this->db->insert($this->table, $data);
        if ($insert) {
            $message = "Data berhasil ditambahkan";
            $code = 200;
            $status = true;
        } else {
            $message = "Data gagal ditambahkan";
            $code = 502;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function index_put() {

        $this->form_validation->set_data($this->put());
        $this->form_validation->set_rules('id', 'supplier id', 'trim|required');
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('address', 'address', 'trim|required');
        $this->form_validation->set_rules('description', 'description', 'trim|required');

        if(!$this->form_validation->run()) {
            $response = array(
                'data'      => null,
                'message'   => strip_tags(validation_errors()),
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $id = $this->put('id');

        $data = array(
                    'id'                    => $this->put('id'),
                    'name'                  => $this->put('name'),
                    'code'                  => $this->put('code'),
                    'phone'                 => $this->put('phone'),
                    'address'               => $this->put('address'),
                    'description'           => $this->put('description'),
                );

        // Get supplier availability
        $this->db->where('id', $data['id']);
        $currentCategory = $this->db->get($this->table)->result();

        $this->db->where('code', $data['code']);
        $category = $this->db->get($this->table)->result();
        if(sizeof($category) >= 1 && $data['code'] != $currentCategory[0]->code) {
            $response = array(
                'data'      => null,
                'message'   => "Supplier with same code already registered",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $this->db->where('id', $id);
        $update = $this->db->update($this->table, $data);
        if ($update) {
            $message = "Edit supplier success";
            $code = 200;
            $status = true;
        } else {
            $message = "Edit supplier failed";
            $code = 502;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }
}
