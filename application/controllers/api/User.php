<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class User extends REST_Controller
{

    protected $table = "karyawan";

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get()
    {
        $id = $this->get('id');
        $type = $this->get('type');
        $query = $this->get('query');

        if ($type != '') {
            $this->db->where('type', $type);
        }

        if ($query != '') {
            $this->db->like('name', $query);
        }

        if ($id == '') {
            $data = $this->db->get($this->table)->result();
        } else {
            $this->db->where('id', $id);
            $data = $this->db->get($this->table)->result();
        }

        if ($data == null || $data == "") {
            $message = "Data tidak ditemukan";
            $code = 404;
            $status = false;
        } else {
            $message = "Data ditemukan";
            $code = 200;
            $status = true;
        }

        $response = array(
            'data'      => $data,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function index_post()
    {
        $data = array(
            'name'      => $this->post('name'),
            'type'      => $this->post('type'),
            'age'       => $this->post('age'),
            'username'  => $this->post('username'),
            'phone'     => $this->post('phone'),
            'address'   => $this->post('address'),
        );
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['password'] = md5("merdeka123");
        $insert = $this->db->insert($this->table, $data);
        if ($insert) {
            $message = "Add user success";
            $code = 200;
            $status = true;
        } else {
            $data = null;
            $message = "Add user failed";
            $code = 502;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        return $this->response($response, $code);
    }

    function login_post()
    {
        $loginParam = array(
            'username'  => $this->post('username'),
            'password'  => $this->post('password'),
        );

        $this->db->select('id, name, type, age, username, phone, address');
        $this->db->where('username', $loginParam['username']);
        $this->db->where('password', md5($loginParam['password']));
        $data = $this->db->get($this->table)->result();

        if ($data) {
            $message = "Login Berhasil";
            $code = 200;
            $status = true;
        } else {
            $message = "Username dan Password tidak valid";
            $code = 401;
            $status = false;
        }

        $response = array(
            'data'      => $data,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function reset_password_post()
    {
        $resetParam = array(
            'username'  => $this->post('username'),
            'phone'     => $this->post('phone'),
        );

        $resetParam['password'] = md5("merdeka123");
        $resetParam['updated_at'] = date("Y-m-d H:i:s");;
        $this->db->where('username', $resetParam['username']);
        $this->db->where('phone', $resetParam['phone']);
        $reset = $this->db->update($this->table, $resetParam);

        if ($reset) {
            $message = "Reset password success, you can login with current default password";
            $code = 200;
            $status = true;
        } else {
            $message = "Username and Phone not match any record";
            $code = 404;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function change_password_post()
    {
        $changeParam = array(
            'id'               => $this->post('id'),
            'old_password'     => $this->post('old_password'),
            'new_password'     => $this->post('new_password'),
        );

        $this->db->where('id', $changeParam['id']);
        $this->db->where('password', md5($changeParam['old_password']));

        $updatedUser = $this->db->get($this->table)->row();
        if (!isset($updatedUser)) {
            $response = array(
                'data'      => null,
                'message'   => "Old password not match with current user",
                'status'    => false,
            );
    
            return $this->response($response, 502);
        }
        $updatedUser->password = md5($changeParam['new_password']);
        $updatedUser->updated_at =  date("Y-m-d H:i:s");;

        $this->db->where('id', $changeParam['id']);
        $this->db->where('password', md5($changeParam['old_password']));
        $change = $this->db->update($this->table, (array) $updatedUser);

        if ($change) {
            $message = "Change password success, you must re-login to apply this change.";
            $code = 200;
            $status = true;
        } else {
            $message = "Change password failed";
            $code = 404;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        return $this->response($response, $code);
    }

    function change_profile_post()
    {
        $changeParam = array(
            'id'            => $this->post('id'),
            'address'       => $this->post('address'),
            'name'          => $this->post('name'),
            'age'           => $this->post('age'),
        );

        $this->db->where('id', $changeParam['id']);
        $updatedUser = $this->db->get($this->table)->row();

        $updatedUser->address = $changeParam['address'];
        $updatedUser->name = $changeParam['name'];
        $updatedUser->age = $changeParam['age'];
        $updatedUser->updated_at = date("Y-m-d H:i:s");;

        $this->db->where('id', $updatedUser->id);
        $change = $this->db->update($this->table, (array) $updatedUser);

        if ($change) {
            $message = "Edit profile success";
            $code = 200;
            $status = true;
        } else {
            $message = "Edit password failed";
            $code = 404;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }
}
