<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Product extends REST_Controller {

    protected $table = "product";
    protected $tableCategory = "product_catagory";
    protected $tableUser = "karyawan";
    protected $tableSupplier = "supplier";

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->database();
    }

     //Menampilkan data kontak
     function index_get() {
        $id = $this->get('id');
        if ($id != '') {
            $this->db->where('id', $id);
        }
        $this->db->order_by('created_at', 'DESC');
        $data = $this->db->get($this->table)->result();

        if($data == null || $data == "") {
            $response = array(
                'data'      => [],
                'message'   => "Data tidak ditemukan",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $message = "Data ditemukan";
        $code = 200;
        $status = true;

        for ($x = 0; $x < sizeof($data); $x++) {
            // Get category of product
            $this->db->where('id', $data[$x]->product_catagory_id);
            $data[$x]->product_catagory = $this->db->get($this->tableCategory)->row();
            unset($data[$x]->product_catagory_id);

            // Get supplier of product
            $this->db->where('code', $data[$x]->supplier_code);
            $data[$x]->supplier = $this->db->get($this->tableSupplier)->row();
            unset($data[$x]->supplier_code);

            // Get user of product
            $this->db->select('id, name, type, age, username, phone, address');
            $this->db->where('id', $data[$x]->user_id);
            $data[$x]->user = $this->db->get($this->tableUser)->row();
            unset($data[$x]->user_id);
        }

        $response = array(
            'data'      => $data,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function lookup_get() {
        $query = $this->get('query');
        if ($query != '') {
            $this->db->like('name', $query);
        }
        $this->db->order_by('created_at', 'DESC');
        $data = $this->db->get($this->table)->result();

        if($data == null || $data == "") {
            $response = array(
                'data'      => [],
                'message'   => "Data tidak ditemukan",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $message = "Data ditemukan";
        $code = 200;
        $status = true;

        for ($x = 0; $x < sizeof($data); $x++) {
            // Get category of product
            $this->db->where('id', $data[$x]->product_catagory_id);
            $data[$x]->product_catagory = $this->db->get($this->tableCategory)->row();
            unset($data[$x]->product_catagory_id);

            // Get supplier of product
            $this->db->where('code', $data[$x]->supplier_code);
            $data[$x]->supplier = $this->db->get($this->tableSupplier)->row();
            unset($data[$x]->supplier_code);

            // Get user of product
            $this->db->select('id, name, type, age, username, phone, address');
            $this->db->where('id', $data[$x]->user_id);
            $data[$x]->user = $this->db->get($this->tableUser)->row();
            unset($data[$x]->user_id);
        }

        $response = array(
            'data'      => $data,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function index_post() {

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('user_id', 'user', 'trim|required|numeric');
        $this->form_validation->set_rules('product_catagory_id', 'Product Category', 'trim|required|numeric');
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('description', 'description', 'trim|required');
        $this->form_validation->set_rules('supplier_code', 'supplier', 'trim|required');
        $this->form_validation->set_rules('buy_price', 'Buy price', 'trim|required|numeric');
        $this->form_validation->set_rules('stock', 'stock', 'trim|required|numeric');
        $this->form_validation->set_rules('sell_price', 'Sell price', 'trim|required|numeric');

        if(!$this->form_validation->run()) {
            $response = array(
                'data'      => null,
                'message'   => strip_tags(validation_errors()),
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $data = array(
                    'user_id'               => $this->post('user_id'),
                    'product_catagory_id'   => $this->post('product_catagory_id'),
                    'name'                  => $this->post('name'),
                    'description'           => $this->post('description'),
                    'supplier_code'         => $this->post('supplier_code'),
                    'buy_price'             => $this->post('buy_price'),
                    'stock'                 => $this->post('stock'),
                    'sell_price'            => $this->post('sell_price'),
                );
        $data['created_at'] = date("Y-m-d H:i:s");

        // Get category availability
        $this->db->where('id', $data['product_catagory_id']);
        $category = $this->db->get($this->tableCategory)->result();
        if($category == null || sizeof($category) < 1) {
            $response = array(
                'data'      => null,
                'message'   => "Invalid product category detected",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        // Get user availability
        $this->db->where('id', $data['user_id']);
        $user = $this->db->get($this->tableUser)->result();
        if($user == null || sizeof($user) < 1) {
            $response = array(
                'data'      => null,
                'message'   => "Invalid user detected",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        // Get supplier availability
        $this->db->where('code', $data['supplier_code']);
        $supplier = $this->db->get($this->tableSupplier)->result();
        if($supplier == null || sizeof($supplier) < 1) {
            $response = array(
                'data'      => null,
                'message'   => "Invalid supplier detected",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $insert = $this->db->insert($this->table, $data);
        if ($insert) {
            $message = "Data berhasil ditambahkan";
            $code = 200;
            $status = true;
        } else {
            $message = "Data gagal ditambahkan";
            $code = 502;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function index_put() {

        $this->form_validation->set_data($this->put());
        $this->form_validation->set_rules('id', 'ID', 'trim|required|numeric');
        $this->form_validation->set_rules('user_id', 'user', 'trim|required|numeric');
        $this->form_validation->set_rules('product_catagory_id', 'Product Category', 'trim|required|numeric');
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('description', 'description', 'trim|required');
        $this->form_validation->set_rules('supplier_code', 'supplier', 'trim|required');
        $this->form_validation->set_rules('buy_price', 'Buy price', 'trim|required|numeric');
        $this->form_validation->set_rules('stock', 'stock', 'trim|required|numeric');
        $this->form_validation->set_rules('sell_price', 'Sell price', 'trim|required|numeric');

        if(!$this->form_validation->run()) {
            $response = array(
                'data'      => null,
                'message'   => strip_tags(validation_errors()),
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $id = $this->put('id');

        $data = array(
                    'id'                    => $this->put('id'),
                    'user_id'               => $this->put('user_id'),
                    'product_catagory_id'   => $this->put('product_catagory_id'),
                    'name'                  => $this->put('name'),
                    'description'           => $this->put('description'),
                    'supplier_code'         => $this->put('supplier_code'),
                    'buy_price'             => $this->put('buy_price'),
                    'stock'                 => $this->put('stock'),
                    'sell_price'            => $this->put('sell_price'),
                );
        $data['updated_at'] = date("Y-m-d H:i:s");

        // Get category availability
        $this->db->where('id', $data['product_catagory_id']);
        $category = $this->db->get($this->tableCategory)->result();
        if($category == null || sizeof($category) < 1) {
            $response = array(
                'data'      => null,
                'message'   => "Invalid product category detected",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        // Get user availability
        $this->db->where('id', $data['user_id']);
        $user = $this->db->get($this->tableUser)->result();
        if($user == null || sizeof($user) < 1) {
            $response = array(
                'data'      => null,
                'message'   => "Invalid user detected",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        // Get supplier availability
        $this->db->where('code', $data['supplier_code']);
        $supplier = $this->db->get($this->tableSupplier)->result();
        if($supplier == null || sizeof($supplier) < 1) {
            $response = array(
                'data'      => null,
                'message'   => "Invalid supplier detected",
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $this->db->where('id', $id);
        $update = $this->db->update($this->table, $data);
        if ($update) {
            $message = "Data berhasil diubah";
            $code = 200;
            $status = true;
        } else {
            $message = "Data gagal diubah";
            $code = 502;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function index_delete() {
        $this->form_validation->set_data($this->delete());
        $this->form_validation->set_rules('id', 'product ID', 'trim|required|numeric');

        if(!$this->form_validation->run()) {
            $response = array(
                'data'      => null,
                'message'   => strip_tags(validation_errors()),
                'status'    => false,
            );

            return $this->response($response, 404);
        }

        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete($this->table);
        if ($delete) {
            $message = "Data berhasil dihapus";
            $code = 200;
            $status = true;
        } else {
            $message = "Data gagal dihapus";
            $code = 502;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }
}
