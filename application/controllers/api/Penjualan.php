<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Penjualan extends REST_Controller
{

    protected $table = "penjualan";
    protected $tableDetail = "penjualan_detail";
    protected $tableProduct = "product";
    protected $tableUser = "karyawan";

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->database();
    }

    //Menampilkan data supplier
    function index_get()
    {
        $id = $this->get('id');
        if ($id != '') {
            $this->db->where('id', $id);
        }

        $this->db->order_by('created_at', 'DESC');
        $data = $this->db->get($this->table)->result();

        if ($data == null || $data == "") {
            $response = array(
                'data' => [],
                'message' => "Data tidak ditemukan",
                'status' => false,
            );

            return $this->response($response, 404);
        }

        $i = 0;
        foreach ($data as $item) {

            // Get User detail data
            $this->db->where('id', $item->id_karyawan);
            $user = $this->db->get($this->tableUser)->row();

            $data[$i]->karyawan = $user;

            $this->db->where('id_penjualan', $item->id);
            $child = $this->db->get($this->tableDetail)->result();

            $data[$i]->item = $child;
            for ($x = 0; $x < sizeof($child); $x++) {
                // Get product detail for child item
                $this->db->where('id', $child[$x]->id_product);
                $product = $this->db->get($this->tableProduct)->result();
                if (!isset($product) || sizeof($product) < 1) {
                    $response = array(
                        'data' => null,
                        'message' => "Report pembelian detail not found, error occured while trying to get product detail",
                        'status' => true,
                    );

                    $this->response($response, 404);
                    return;
                }

                unset($product[0]->created_at);
                unset($product[0]->updated_at);
                unset($product[0]->deleted_at);
                $child[$x]->product = $product[0];

                unset($child[$x]->id_product);
                unset($child[$x]->created_at);
                unset($child[$x]->updated_at);
                unset($child[$x]->deleted_at);
            }
            $i++;
        }

        $message = "Data ditemukan";
        $code = 200;
        $status = true;

        $response = array(
            'data' => $data,
            'message' => $message,
            'status' => $status,
        );

        $this->response($response, $code);
    }

    function index_post()
    {

        $inputJSON = json_decode($this->input->raw_input_stream, true);
        $itemChild = $inputJSON['items'];

        foreach ($itemChild as $child) {
            $this->form_validation->set_rules("id_product", "Product", "required|trim|numeric");
            $this->form_validation->set_rules("quantity", "Quantity", "required|trim|numeric");
            $this->form_validation->set_rules("price", "Price", "required|trim|numeric");
            $this->form_validation->set_data($child);
            if (!$this->form_validation->run()) {
                $response = array(
                    'data' => null,
                    'message' => strip_tags(validation_errors()),
                    'status' => false,
                );

                return $this->response($response, 404);
            }
        }

        if (!isset($inputJSON) || !isset($itemChild) || sizeof($itemChild) < 1) {
            $response = array(
                'data' => null,
                'message' => "Invalid data detected",
                'status' => false,
            );

            return $this->response($response, 404);
        }

        $paymentReceived = $inputJSON['payment'];
        if (!isset($paymentReceived)) {
            $paymentReceived = 0;
        }

        $data = array(
            'id_karyawan' => $inputJSON['id_karyawan'],
            'total_price' => 0,
            'payment_received' => $paymentReceived,
            'payment_changes' => 0,
            'customer_name' => $inputJSON['customer_name'],
            'created_at' => date("Y-m-d H:i:s"),
        );

        $insert = $this->db->insert($this->table, $data);
        if ($insert) {
            $insertId = $this->db->insert_id();
            $i = 0;
            $updatedProducts = [];
            $totalTransactionPrice = 0;
            foreach ($itemChild as $child) {
                # Select data from table product to check stock
                $this->db->where('id', $child['id_product']);
                $product = $this->db->get($this->tableProduct)->result();
                $stockAfterSell = $product[0]->stock - $child['quantity'];
                $isOutOfStock = $stockAfterSell < 0;
                if ($isOutOfStock) {
                    $this->db->where('id', $insertId);
                    $this->db->delete($this->table);

                    $response = array(
                        'data' => null,
                        'message' => "Not enough or empty stock detected for product ". $product[0]->name .", transaction canceled",
                        'status' => false,
                    );
    
                    return $this->response($response, 404);
                }

                $product[0]->stock = $stockAfterSell;
                # End checking stock and update stock with stockAfterSell value

                $itemChild[$i]['id_penjualan'] = $insertId;
                $itemChild[$i]['created_at'] = date("Y-m-d H:i:s");
                $updatedProducts[$i] = (array) $product[0];
                $totalTransactionPrice += (int) $child['quantity'] * (int) $child['price'];
                $itemChild[$i]['price'] = $totalTransactionPrice;
                $i++;
            }

            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 
            $this->db->insert_batch($this->tableDetail, $itemChild);
            # Update affected stocks in product table
            foreach($updatedProducts as $updatedProduct) {
                $this->db->where('id', $updatedProduct['id']);
                $this->db->update($this->tableProduct, $updatedProduct);
            }

            if ($paymentReceived < $totalTransactionPrice) {
                # Total payment is less than total transaction price.
                $this->db->trans_rollback();
                $this->db->where('id', $insertId);
                $this->db->delete($this->table);
                $message = "Invalid payment detected";
                $code = 501;
                $status = false;
                $data = null;

                $response = array(
                    'data' => $data,
                    'message' => $message,
                    'status' => $status,
                );

                return $this->response($response, $code);
            }

            # update total price in head table penjualan
            $paymentChanges = $paymentReceived - $totalTransactionPrice;
            $this->db->set('total_price', $totalTransactionPrice, false);
            $this->db->set('payment_changes', $paymentChanges, false);
            $this->db->where('id', $insertId);
            $this->db->update($this->table);

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
                $this->db->where('id', $insertId);
                $this->db->delete($this->table);
                $message = "Data gagal ditambahkan";
                $code = 502;
                $status = false;
                $data = null;
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
                $message = "Data berhasil ditambahkan";
                $code = 200;
                $status = true;

                $data = array(
                    'invoice_id'    => $insertId,
                );
            }
        } else {
            $message = "Data gagal ditambahkan";
            $code = 502;
            $status = false;
            $data = null;
        }

        $response = array(
            'data' => $data,
            'message' => $message,
            'status' => $status,
        );

        $this->response($response, $code);
    }

    function print_invoice_get() {
        $id = $this->get('invoice_id');
        $this->db->where('id', $id);
        $headData = $this->db->get($this->table)->row();
        if(!isset($headData)) {
            $response = array(
                'data' => null,
                'message' => "Invoice data not found",
                'status' => false,
            );

            return $this->response($response, 404);
        }

        // Get User detail data
        $this->db->where('id', $headData->id_karyawan);
        $user = $this->db->get($this->tableUser)->row();

        $headData->karyawan = $user;

        $this->db->where('id_penjualan', $headData->id);
        $childData = $this->db->get($this->tableDetail)->result();
        for ($x = 0; $x < sizeof($childData); $x++) {
            // Get product detail for child item
            $this->db->where('id', $childData[$x]->id_product);
            $product = $this->db->get($this->tableProduct)->result();
            if (!isset($product) || sizeof($product) < 1) {
                $response = array(
                    'data' => null,
                    'message' => "Invoice detail not found, error occured while trying to get product detail",
                    'status' => true,
                );
        
                return $this->response($response, 404);
            }
            unset($product[0]->created_at);
            unset($product[0]->updated_at);
            unset($product[0]->deleted_at);
            $childData[$x]->product = $product[0];
            
            unset($childData[$x]->id_product);
            unset($childData[$x]->id_penjualan);
            unset($childData[$x]->created_at);
            unset($childData[$x]->updated_at);
            unset($childData[$x]->deleted_at);
        }

        $headData->item = $childData;
        $response = array(
            'data' => $headData,
            'message' => "Invoice detail result",
            'status' => true,
        );

        return $this->response($response, 200);
    }
}