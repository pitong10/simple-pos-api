<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class ProductCategory extends REST_Controller {

    protected $table = "product_catagory";

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->database();
    }

     function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $data = $this->db->get($this->table)->result();
        } else {
            $this->db->where('id', $id);
            $data = $this->db->get($this->table)->result();
        }

        if($data == null || $data == "") {
            $message = "Data tidak ditemukan";
            $code = 404;
            $status = false;
        } else {
            $message = "Data ditemukan";
            $code = 200;
            $status = true;
        }

        $response = array(
            'data'      => $data,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function lookup_get() {
        $query = $this->get('query');
        if ($query != '') {
            $this->db->like('name', $query);
        }
        $this->db->order_by('name', 'ASC');
        $data = $this->db->get($this->table)->result();

        if($data == null || $data == "") {
            $message = "Data tidak ditemukan";
            $code = 404;
            $status = false;
        } else {
            $message = "Data ditemukan";
            $code = 200;
            $status = true;
        }

        $response = array(
            'data'      => $data,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function index_post() {
        $data = array(
                    'name'          => $this->post('name'),
                    'description'   => $this->post('description'),
                );
                
        $insert = $this->db->insert($this->table, $data);
        if ($insert) {
            $message = "Data berhasil ditambahkan";
            $code = 200;
            $status = true;
        } else {
            $message = "Data gagal ditambahkan";
            $code = 502;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function index_put() {
        $id = $this->put('id');
        $data = array(
            'id'            => $this->put('id'),
            'name'          => $this->put('name'),
            'description'   => $this->put('description'),
        );
        $this->db->where('id', $id);
        $update = $this->db->update($this->table, $data);
        if ($update) {
            $message = "Data berhasil diupdate";
            $code = 200;
            $status = true;
        } else {
            $message = "Data gagal diupdate";
            $code = 502;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

    function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete($this->table);
        if ($delete) {
            $message = "Data berhasil dihapus";
            $code = 200;
            $status = true;
        } else {
            $message = "Data gagal dihapus";
            $code = 502;
            $status = false;
        }

        $response = array(
            'data'      => null,
            'message'   => $message,
            'status'    => $status,
        );

        $this->response($response, $code);
    }

}
