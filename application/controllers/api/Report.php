<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Report extends REST_Controller
{

    protected $tablePembelian = "pembelian";
    protected $tablePembelianDetail = "pembelian_detail";
    protected $tablePenjualan = "penjualan";
    protected $tablePenjualanDetail = "penjualan_detail";
    protected $tableProduct = "product";
    protected $tableUser = "karyawan";
    protected $tableSupplier = "supplier";
    protected $tableCategory = "product_catagory";

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->database();
    }

    function pembelian_post()
    {
        $queryJSON = json_decode($this->input->raw_input_stream, true);
        if (
            !array_key_exists('id_karyawan', $queryJSON) ||
            !array_key_exists('start_date', $queryJSON) ||
            !array_key_exists('end_date', $queryJSON)
        ) {
            $response = array(
                'data' => null,
                'message' => "Invalid report filter applied",
                'status' => true,
            );

            $this->response($response, 404);
            return;
        }
        $start_date = $queryJSON['start_date'];
        $end_date = $queryJSON['end_date'];
        $id_karyawan = $queryJSON['id_karyawan'];

        if (isset($id_karyawan) && $id_karyawan >= 1)
            $this->db->where('id_karyawan', $id_karyawan);

        if ($start_date && $start_date != "")
            $this->db->where('created_at >=', $start_date);

        if (isset($end_date) && $end_date != "")
            $this->db->where('created_at <=', $end_date);

        $headData = $this->db->get($this->tablePembelian)->result();
        if (!isset($headData) || sizeof($headData) < 1) {
            $response = array(
                'data' => null,
                'message' => "Report pembelian data not found",
                'status' => false,
            );

            return $this->response($response, 404);
        }
        for ($i = 0; $i < sizeof($headData); $i++) {
            $this->db->where('id_pembelian', $headData[$i]->id);
            $childData = $this->db->get($this->tablePembelianDetail)->result();
            for ($x = 0; $x < sizeof($childData); $x++) {
                // Get product detail for child item
                $this->db->where('id', $childData[$x]->id_product);
                $product = $this->db->get($this->tableProduct)->result();
                if (!isset($product) || sizeof($product) < 1) {
                    $response = array(
                        'data' => null,
                        'message' => "Report pembelian detail not found, error occured while trying to get product detail",
                        'status' => true,
                    );

                    $this->response($response, 404);
                    return;
                }

                unset($product[0]->created_at);
                unset($product[0]->updated_at);
                unset($product[0]->deleted_at);
                $childData[$x]->product = $product[0];

                unset($childData[$x]->id_product);
                unset($childData[$x]->id_penjualan);
                unset($childData[$x]->created_at);
                unset($childData[$x]->updated_at);
                unset($childData[$x]->deleted_at);
            }

            $headData[$i]->items = $childData;
        }

        $response = array(
            'data' => $headData,
            'message' => "Report pembelian detail result from " . $start_date . " until " . $end_date,
            'status' => true,
        );

        return $this->response($response, 200);
    }

    function penjualan_post()
    {
        $queryJSON = json_decode($this->input->raw_input_stream, true);
        if (
            !array_key_exists('id_karyawan', $queryJSON) ||
            !array_key_exists('start_date', $queryJSON) ||
            !array_key_exists('end_date', $queryJSON)
        ) {
            $response = array(
                'data' => null,
                'message' => "Invalid report filter applied",
                'status' => true,
            );

            $this->response($response, 404);
            return;
        }
        $start_date = $queryJSON['start_date'];
        $end_date = $queryJSON['end_date'];
        $id_karyawan = $queryJSON['id_karyawan'];

        if (isset($id_karyawan) && $id_karyawan >= 1)
            $this->db->where('id_karyawan', $id_karyawan);

        if ($start_date && $start_date != "")
            $this->db->where('created_at >=', $start_date);

        if (isset($end_date) && $end_date != "")
            $this->db->where('created_at <=', $end_date);

        $headData = $this->db->get($this->tablePenjualan)->result();
        if (!isset($headData) || sizeof($headData) < 1) {
            $response = array(
                'data' => null,
                'message' => "Report penjualan data not found",
                'status' => false,
            );

            return $this->response($response, 404);
        }
        for ($i = 0; $i < sizeof($headData); $i++) {
            $this->db->where('id_penjualan', $headData[$i]->id);
            $childData = $this->db->get($this->tablePenjualanDetail)->result();
            for ($x = 0; $x < sizeof($childData); $x++) {
                // Get product detail for child item
                $this->db->where('id', $childData[$x]->id_product);
                $product = $this->db->get($this->tableProduct)->result();
                if (!isset($product) || sizeof($product) < 1) {
                    $response = array(
                        'data' => null,
                        'message' => "Report penjualan detail not found, error occured while trying to get product detail",
                        'status' => true,
                    );

                    $this->response($response, 404);
                    return;
                }

                unset($product[0]->created_at);
                unset($product[0]->updated_at);
                unset($product[0]->deleted_at);
                $childData[$x]->product = $product[0];

                unset($childData[$x]->id_product);
                unset($childData[$x]->id_penjualan);
                unset($childData[$x]->created_at);
                unset($childData[$x]->updated_at);
                unset($childData[$x]->deleted_at);
            }

            $headData[$i]->items = $childData;
        }

        $response = array(
            'data' => $headData,
            'message' => "Report penjualan detail result from " . $start_date . " until " . $end_date,
            'status' => true,
        );

        return $this->response($response, 200);
    }

    function product_post()
    {
        $queryJSON = json_decode($this->input->raw_input_stream, true);
        if (
            !array_key_exists('start_date', $queryJSON) ||
            !array_key_exists('end_date', $queryJSON)
        ) {
            $response = array(
                'data' => null,
                'message' => "Invalid report filter applied",
                'status' => true,
            );

            $this->response($response, 404);
            return;
        }
        $start_date = $queryJSON['start_date'];
        $end_date = $queryJSON['end_date'];

        if ($start_date && $start_date != "")
            $this->db->where('created_at >=', $start_date);

        if (isset($end_date) && $end_date != "")
            $this->db->where('created_at <=', $end_date);

        $headData = $this->db->get($this->tableProduct)->result();
        if (!isset($headData) || sizeof($headData) < 1) {
            $response = array(
                'data' => null,
                'message' => "Report product data not found",
                'status' => false,
            );

            return $this->response($response, 404);
        }
        
        for ($x = 0; $x < sizeof($headData); $x++) {
            // Get category of product
            $this->db->where('id', $headData[$x]->product_catagory_id);
            $headData[$x]->product_catagory = $this->db->get($this->tableCategory)->row();
            unset($headData[$x]->product_catagory_id);

            // Get supplier of product
            $this->db->where('code', $headData[$x]->supplier_code);
            $headData[$x]->supplier = $this->db->get($this->tableSupplier)->row();
            unset($headData[$x]->supplier_code);

            // Get user of product
            $this->db->select('id, name, type, age, username, phone, address');
            $this->db->where('id', $headData[$x]->user_id);
            $headData[$x]->user = $this->db->get($this->tableUser)->row();
            unset($headData[$x]->user_id);
        }

        $response = array(
            'data' => $headData,
            'message' => "Report penjualan detail result from " . $start_date . " until " . $end_date,
            'status' => true,
        );

        return $this->response($response, 200);
    }

    function laba_rugi_post()
    {
        $queryJSON = json_decode($this->input->raw_input_stream, true);
        if (
            !array_key_exists('id_karyawan', $queryJSON) ||
            !array_key_exists('start_date', $queryJSON) ||
            !array_key_exists('end_date', $queryJSON)
        ) {
            $response = array(
                'data' => null,
                'message' => "Invalid report filter applied",
                'status' => true,
            );

            $this->response($response, 404);
            return;
        }
        $start_date = $queryJSON['start_date'];
        $end_date = $queryJSON['end_date'];
        $id_karyawan = $queryJSON['id_karyawan'];

        $period = new DatePeriod(
            new DateTime($start_date),
            new DateInterval('P1D'),
            new DateTime($end_date)
        );

        $report = array();
        $index = 0;
        foreach ($period as $key => $value) {
            $day = $value->format('Y-m-d');

            $this->db->where('DATE(created_at)', $day);
            $totalPembelianDay = 0;
            $pembelianInDay = $this->db->get($this->tablePembelian)->result();
            for ($x = 0; $x < sizeof($pembelianInDay); $x++) {
                $totalPembelianDay += $pembelianInDay[$x]->total_price;
            }

            $totalPenjualanDay = 0;
            $this->db->where('DATE(created_at)', $day);
            $penjualanInDay = $this->db->get($this->tablePenjualan)->result();
            for ($x = 0; $x < sizeof($penjualanInDay); $x++) {
                $totalPenjualanDay += $penjualanInDay[$x]->total_price;
            }

            $data = new stdClass();
            $data->date = $day;
            $data->pembelian = $totalPembelianDay;
            $data->penjualan = $totalPenjualanDay;
            $data->pendapatan = $totalPenjualanDay - $totalPembelianDay;

            $report[$index] = $data;
            $index++;
        }

        $response = array(
            'data' => $report,
            'message' => "Report profit and loss detail result from " . $start_date . " until " . $end_date,
            'status' => true,
        );

        return $this->response($response, 200);
    }

    function pdf_penjualan_get()
    {
        $start_date =  $this->get('start_date');
        $end_date =  $this->get('end_date');
        $id_karyawan = $this->get('id_karyawan');

        if (isset($id_karyawan) && $id_karyawan >= 1)
            $this->db->where('id_karyawan', $id_karyawan);

        $prefix = "";
        if ($start_date && $start_date != "") {
            $prefix = date('d M y', strtotime($start_date));
            $this->db->where('created_at >=', $start_date);
        }

        $suffix = "";
        if (isset($end_date) && $end_date != "") {
            $suffix = " s/d " . date('d M y', strtotime($end_date));
            $this->db->where('created_at <=', $end_date);
        }

        $headData = $this->db->get($this->tablePenjualan)->result();
        if (!isset($headData) || sizeof($headData) < 1) {
            $response = array(
                'data' => null,
                'message' => "Report penjualan data not found",
                'status' => false,
            );

            return $this->response($response, 404);
        }
        for ($i = 0; $i < sizeof($headData); $i++) {
            // Get karyawan detail for child item
            $this->db->where('id', $headData[$i]->id_karyawan);
            $user = $this->db->get($this->tableUser)->row();
            if (!isset($user)) {
                $response = array(
                    'data' => null,
                    'message' => "Report penjualan user not found, error occured while trying to get user detail",
                    'status' => true,
                );

                $this->response($response, 404);
                return;
            }

            unset($headData[$i]->id_karyawan);
            $headData[$i]->karyawan = $user;

            $this->db->where('id_penjualan', $headData[$i]->id);
            $childData = $this->db->get($this->tablePenjualanDetail)->result();
            for ($x = 0; $x < sizeof($childData); $x++) {
                // Get product detail for child item
                $this->db->where('id', $childData[$x]->id_product);
                $product = $this->db->get($this->tableProduct)->result();
                if (!isset($product) || sizeof($product) < 1) {
                    $response = array(
                        'data' => null,
                        'message' => "Report penjualan detail not found, error occured while trying to get product detail",
                        'status' => true,
                    );

                    $this->response($response, 404);
                    return;
                }

                unset($product[0]->created_at);
                unset($product[0]->updated_at);
                unset($product[0]->deleted_at);
                $childData[$x]->product = $product[0];

                unset($childData[$x]->id_product);
                unset($childData[$x]->id_penjualan);
                unset($childData[$x]->created_at);
                unset($childData[$x]->updated_at);
                unset($childData[$x]->deleted_at);
            }

            $headData[$i]->items = $childData;
        }

        $this->load->library('pdf');
        $data['report_title'] = "Laporan Penjualan Periode " . $prefix . $suffix;
        $data['penjualan_items'] = $headData;
        $html = $this->load->view('ReportPenjualanPdfView', $data, true);
        $this->pdf->createPDF($html, 'penjualan_report');
    }

    function pdf_pembelian_get()
    {

        $start_date =  $this->get('start_date');
        $end_date =  $this->get('end_date');
        $id_karyawan = $this->get('id_karyawan');

        if (isset($id_karyawan) && $id_karyawan >= 1)
            $this->db->where('id_karyawan', $id_karyawan);

        $prefix = "";
        if ($start_date && $start_date != "") {
            $prefix = date('d M y', strtotime($start_date));
            $this->db->where('created_at >=', $start_date);
        }

        $suffix = "";
        if (isset($end_date) && $end_date != "") {
            $suffix = " s/d " . date('d M y', strtotime($end_date));
            $this->db->where('created_at <=', $end_date);
        }

        $headData = $this->db->get($this->tablePembelian)->result();
        if (!isset($headData) || sizeof($headData) < 1) {
            $response = array(
                'data' => null,
                'message' => "Report pembelian data not found",
                'status' => false,
            );

            return $this->response($response, 404);
        }
        for ($i = 0; $i < sizeof($headData); $i++) {
            // Get karyawan detail for child item
            $this->db->where('id', $headData[$i]->id_karyawan);
            $user = $this->db->get($this->tableUser)->row();
            if (!isset($user)) {
                $response = array(
                    'data' => null,
                    'message' => "Report pembelian not found, error occured while trying to get user detail",
                    'status' => true,
                );

                $this->response($response, 404);
                return;
            }

            unset($headData[$i]->id_karyawan);
            $headData[$i]->karyawan = $user;

            $this->db->where('id_pembelian', $headData[$i]->id);
            $childData = $this->db->get($this->tablePembelianDetail)->result();
            for ($x = 0; $x < sizeof($childData); $x++) {
                // Get product detail for child item
                $this->db->where('id', $childData[$x]->id_product);
                $product = $this->db->get($this->tableProduct)->result();
                if (!isset($product) || sizeof($product) < 1) {
                    $response = array(
                        'data' => null,
                        'message' => "Report pembelian detail not found, error occured while trying to get product detail",
                        'status' => true,
                    );

                    $this->response($response, 404);
                    return;
                }

                // Get supplier detail for child item
                $this->db->where('id', $childData[$x]->id_supplier);
                $supplier = $this->db->get($this->tableSupplier)->row();
                if (!isset($supplier)) {
                    $response = array(
                        'data' => null,
                        'message' => "Report pembelian not found, error occured while trying to get supplier detail",
                        'status' => true,
                    );

                    $this->response($response, 404);
                    return;
                }

                unset($childData[$x]->id_supplier);
                $childData[$x]->supplier = $supplier;

                unset($product[0]->created_at);
                unset($product[0]->updated_at);
                unset($product[0]->deleted_at);
                $childData[$x]->product = $product[0];

                unset($childData[$x]->id_product);
                unset($childData[$x]->id_penjualan);
                unset($childData[$x]->created_at);
                unset($childData[$x]->updated_at);
                unset($childData[$x]->deleted_at);
            }

            $headData[$i]->items = $childData;
        }

        $this->load->library('pdf');
        $data['report_title'] = "Laporan Pembelian Periode " . $prefix . $suffix;
        $data['pembelian_items'] = $headData;
        $html = $this->load->view('ReportPembelianPdfView', $data, true);
        $this->pdf->createPDF($html, 'pembelian_report');
    }

    function pdf_product_get()
    {
        $start_date =  $this->get('start_date');
        $end_date =  $this->get('end_date');

        $prefix = "";
        if ($start_date && $start_date != "") {
            $prefix = date('d M y', strtotime($start_date));
            $this->db->where('created_at >=', $start_date);
        }

        $suffix = "";
        if (isset($end_date) && $end_date != "") {
            $suffix = " s/d " . date('d M y', strtotime($end_date));
            $this->db->where('created_at <=', $end_date);
        }

        $headData = $this->db->get($this->tableProduct)->result();
        if (!isset($headData) || sizeof($headData) < 1) {
            $response = array(
                'data' => null,
                'message' => "Report product data not found",
                'status' => false,
            );

            return $this->response($response, 404);
        }
        
        for ($x = 0; $x < sizeof($headData); $x++) {
            // Get category of product
            $this->db->where('id', $headData[$x]->product_catagory_id);
            $headData[$x]->product_catagory = $this->db->get($this->tableCategory)->row();
            unset($headData[$x]->product_catagory_id);

            // Get supplier of product
            $this->db->where('code', $headData[$x]->supplier_code);
            $headData[$x]->supplier = $this->db->get($this->tableSupplier)->row();
            unset($headData[$x]->supplier_code);

            // Get user of product
            $this->db->select('id, name, type, age, username, phone, address');
            $this->db->where('id', $headData[$x]->user_id);
            $headData[$x]->user = $this->db->get($this->tableUser)->row();
            unset($headData[$x]->user_id);
        }

        $this->load->library('pdf');
        $data['report_title'] = "Laporan Produk Periode " . $prefix . $suffix;
        $data['products'] = $headData;
        $html = $this->load->view('ReportProductPdfView', $data, true);
        $this->pdf->createPDF($html, 'product_report');
    }

    function laba_rugi_get()
    {
        $start_date =  $this->get('start_date');
        $end_date =  $this->get('end_date');
        $id_karyawan = $this->get('id_karyawan');

        $prefix = "";
        if ($start_date && $start_date != "") {
            $prefix = date('d M y', strtotime($start_date));
        }

        $suffix = "";
        if (isset($end_date) && $end_date != "") {
            $suffix = " s/d " . date('d M y', strtotime($end_date));
        }

        $period = new DatePeriod(
            new DateTime($start_date),
            new DateInterval('P1D'),
            new DateTime($end_date)
        );

        $report = array();
        $index = 0;
        foreach ($period as $key => $value) {
            $day = $value->format('Y-m-d');

            $this->db->where('DATE(created_at)', $day);
            $totalPembelianDay = 0;
            $pembelianInDay = $this->db->get($this->tablePembelian)->result();
            for ($x = 0; $x < sizeof($pembelianInDay); $x++) {
                $totalPembelianDay += $pembelianInDay[$x]->total_price;
            }

            $totalPenjualanDay = 0;
            $this->db->where('DATE(created_at)', $day);
            $penjualanInDay = $this->db->get($this->tablePenjualan)->result();
            for ($x = 0; $x < sizeof($penjualanInDay); $x++) {
                $totalPenjualanDay += $penjualanInDay[$x]->total_price;
            }

            $data = new stdClass();
            $data->date = $day;
            $data->pembelian = $totalPembelianDay;
            $data->penjualan = $totalPenjualanDay;
            $data->pendapatan = $totalPenjualanDay - $totalPembelianDay;

            $report[$index] = $data;
            $index++;
        }

        $this->load->library('pdf');
        $data = array();
        $data['report_title'] = "Laporan Pendapatan Periode " . $prefix . $suffix;
        $data['report'] = $report;
        $html = $this->load->view('ReportProfitLossPdfView', $data, true);
        $this->pdf->createPDF($html, 'profit_loss_report');
    }
}