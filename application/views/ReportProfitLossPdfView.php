<!DOCTYPE html>
<html>

<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title>Create PDF from View in CodeIgniter Example</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css"
        rel="stylesheet" />
    <style>
        table,
        td,
        th {
            border: 1px solid;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        td-none {
            border: none;
        }
    </style>
</head>

<body>
    <center>
        <div><img src="<?php echo base_url('images/logo-app.png'); ?>" width="240" height="140" /></div>
        <div>Jln K.H Abdul Hadi Cijawa No.3, Cipare, Kec. Serang, Kota Serang, Banten 42117</div>
        <br />
        <br />
        <br />
    </center>
    <h2 class="text-center bg-info"><?php echo $report_title; ?></h2>
    <table class="table">
        <thead>
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Penjualan</th>
                <th>Pembelian</th>
                <th>Pendapatan</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            $grand_total = 0;
            foreach ($report as $item) {
                echo "
                <tr>
                    <td>" . $no . "</td>
                    <td>" . date('d M Y', strtotime($item->date)) . "</td>
                    <td>" . $item->penjualan . "</td>
                    <td>" . $item->pembelian . "</td>
                    <td><div align='right'>" . $item->pendapatan . "</div></td>
                </tr>
                ";
                $no++;
                $grand_total += $item->pendapatan;
            }

            echo "
                <tr>
                    <td colspan=4><div align='center'><b>Grand Total</b></div></td>
                    <td><div align='right'><b>" . $grand_total . "</div></td>
                </tr>
                ";
            ?>
        <tbody>
    </table>
</body>

</html>