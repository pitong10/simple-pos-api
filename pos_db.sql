-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2023 at 03:50 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_db`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`asabem`@`localhost` PROCEDURE `news_dislike` (IN `val_id` INT(11) UNSIGNED, IN `val_userid` INT(11) UNSIGNED)   BEGIN
	UPDATE news SET liked = liked - 1 WHERE id = val_id;
    DELETE FROM  history_like WHERE news_id = val_id AND users_id = val_userid;
END$$

CREATE DEFINER=`asabem`@`localhost` PROCEDURE `news_like` (IN `val_id` INT(11) UNSIGNED, IN `val_userid` INT(11) UNSIGNED)   BEGIN

    UPDATE news SET liked = liked + 1 WHERE id = val_id;

    INSERT INTO history_like(news_id,type,users_id) VALUES (val_id,'news',val_userid);

END$$

CREATE DEFINER=`asabem`@`localhost` PROCEDURE `news_viewed` (IN `val_id` INT(11))   UPDATE news SET viewed = viewed + 1 WHERE id = val_id$$

CREATE DEFINER=`asabem`@`localhost` PROCEDURE `product_dislike` (IN `val_id` INT(11) UNSIGNED, IN `users_id` INT(11) UNSIGNED)   BEGIN
	UPDATE product SET liked = liked - 1 WHERE id = val_id;
    DELETE FROM  history_like WHERE product_id = val_id AND users_id = val_userid;
    
END$$

CREATE DEFINER=`asabem`@`localhost` PROCEDURE `product_like` (IN `val_id` INT(11) UNSIGNED, IN `product_id` INT(11) UNSIGNED)   BEGIN

	UPDATE product SET liked = liked + 1 WHERE id = val_id;
    
    INSERT INTO history_like(product_id,type,users_id) VALUES (val_id,'product',val_userid);
    
END$$

CREATE DEFINER=`asabem`@`localhost` PROCEDURE `product_viewed` (IN `val_id` INT(11))   UPDATE product SET viewed = viewed + 1 WHERE id = val_id$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups`
--

CREATE TABLE `auth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `auth_groups`
--

INSERT INTO `auth_groups` (`id`, `name`, `description`) VALUES
(1, 'admin-event', 'Admin Event'),
(2, 'student', 'Unair Student'),
(3, 'guest', 'Guest'),
(4, 'superadmin', 'Superadmin'),
(5, 'admin-marketplaces', 'Admin Online Store'),
(6, 'admin-posting', 'Admin Information');

-- --------------------------------------------------------

--
-- Table structure for table `auth_permissions`
--

CREATE TABLE `auth_permissions` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `auth_permissions`
--

INSERT INTO `auth_permissions` (`id`, `name`, `description`) VALUES
(1, 'manage-user', 'View and Manage User registration'),
(2, 'manage-system', 'Manange system configurations'),
(3, 'manage-report', 'View and Manage Reporting'),
(4, 'manage-product', 'View and Manage Product'),
(5, 'manage-profile', 'View and Manage Profile/Information'),
(6, 'manage-payment', 'Manage Payment'),
(7, 'manage-store', 'View and Manage Online Store'),
(8, 'manage-event', 'View and Manage Event'),
(9, 'manage-news', 'View and Manage News'),
(10, 'access-mobile', 'Access to Android '),
(11, 'access-website', 'Access to website'),
(12, 'manage-group', 'Manage Group'),
(13, 'create', 'Create/Add Data'),
(14, 'edit', 'Edit/Update Data'),
(15, 'delete', 'Delete Data'),
(16, 'download', 'Download Data'),
(18, 'approve', 'Approve Data'),
(19, 'view-eoj', 'View event, Open Recruitment, Job vacancy'),
(20, 'view-info', 'View Information UNAIR, BEM, ORMAWA'),
(21, 'view-olshop', 'View Online Store'),
(22, 'manage-suggestion', 'Manage Kritik dan Saran');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `type` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `name`, `type`, `age`, `username`, `password`, `phone`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BARU MUNCUL', 1, 30, 'baru', 'd4e8e64db20cf66905b04f8e01e6e5ab', '1123546456', 'Bekasi', '2023-05-22 12:11:10', '2023-05-24 14:22:39', NULL),
(2, 'ELA', 1, 19, 'ela', 'd4e8e64db20cf66905b04f8e01e6e5ab', '748239764234', 'Surabaya', '2023-05-22 12:11:47', NULL, NULL),
(3, 'PITONG', 1, 26, 'pitong', 'd4e8e64db20cf66905b04f8e01e6e5ab', '119119119119', 'Mojokerto', '2023-05-22 12:12:33', '2023-05-24 14:46:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `total_price` bigint(20) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id`, `id_karyawan`, `total_price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 0, '2023-05-20 04:58:07', '2023-05-20 04:58:07', '2023-05-20 04:58:07'),
(11, 3, 0, '2023-05-20 07:12:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 3, 0, '2023-05-20 09:29:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 3, 43500000, '2023-05-20 09:35:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 3, 43500000, '2023-05-20 14:06:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 3, 43500000, '2023-05-20 14:06:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 3, 9500000, '2023-05-22 14:01:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `price` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id`, `id_pembelian`, `id_supplier`, `id_product`, `quantity`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 125, 1, 9500000, '2023-05-20 04:58:46', '2023-05-20 04:58:46', '2023-05-20 04:58:46'),
(2, 1, 1, 126, 1, 8500000, '2023-05-20 04:58:46', '2023-05-20 04:58:46', '2023-05-20 04:58:46'),
(11, 11, 1, 125, 1, 9500000, '2023-05-20 07:12:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 11, 1, 126, 2, 17000000, '2023-05-20 07:12:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 13, 1, 125, 1, 9500000, '2023-05-20 09:29:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 13, 1, 126, 2, 17000000, '2023-05-20 09:29:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 14, 1, 125, 1, 9500000, '2023-05-20 09:35:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 14, 1, 126, 2, 17000000, '2023-05-20 09:35:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 15, 1, 125, 1, 9500000, '2023-05-20 14:06:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 15, 1, 126, 2, 17000000, '2023-05-20 14:06:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 16, 1, 125, 1, 9500000, '2023-05-20 14:06:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 16, 1, 126, 2, 17000000, '2023-05-20 14:06:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 17, 1, 125, 1, 9500000, '2023-05-22 14:01:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `total_price` bigint(20) NOT NULL,
  `customer_name` varchar(100) NOT NULL DEFAULT 'NN',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id`, `id_karyawan`, `total_price`, `customer_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 9500000, 'NN', '2023-05-20 09:11:28', '2023-05-20 09:11:28', '2023-05-20 09:11:28'),
(4, 3, 43500000, 'NN', '2023-05-20 09:24:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 3, 9500000, 'NN', '2023-05-22 14:06:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 3, 9500000, 'NN', '2023-05-27 03:44:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 3, 9500000, 'Eko', '2023-05-27 03:48:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id` int(11) NOT NULL,
  `id_penjualan` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `price` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id`, `id_penjualan`, `id_product`, `quantity`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 125, 1, 9500000, '2023-05-20 09:12:06', '2023-05-20 09:12:06', '2023-05-20 09:12:06'),
(2, 4, 125, 1, 9500000, '2023-05-20 09:24:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 4, 126, 2, 17000000, '2023-05-20 09:24:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 5, 125, 1, 9500000, '2023-05-22 14:06:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 6, 125, 1, 9500000, '2023-05-27 03:44:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 7, 125, 1, 9500000, '2023-05-27 03:48:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `product_catagory_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier_code` varchar(100) NOT NULL,
  `buy_price` bigint(20) NOT NULL,
  `stock` bigint(20) NOT NULL DEFAULT 0,
  `sell_price` bigint(20) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `user_id`, `product_catagory_id`, `name`, `description`, `supplier_code`, `buy_price`, `stock`, `sell_price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(125, 3, 1, 'Macbook Pro 2018', 'Macbook Pro 2018 second IBox warranty', 'KK', 8000000, 5, 9500000, '2023-05-18 12:18:51', NULL, NULL),
(126, 3, 1, 'Macbook Pro 2017', 'Macbook Pro 2017 second', 'KK', 7000000, 11, 8500000, '2023-05-19 11:18:51', NULL, NULL),
(127, 3, 1, 'Logitech G304', 'Mouse gaming dari brand logitech terbaru.', 'KK', 500000, 50, 700000, '2023-05-22 13:12:07', '2023-05-22 13:12:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_catagory`
--

CREATE TABLE `product_catagory` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `product_catagory`
--

INSERT INTO `product_catagory` (`id`, `name`, `description`) VALUES
(1, 'Elektronik', 'Ini berisi barang-barang elektronik yang akan dijual'),
(2, 'Handphone dan Aksesoris', NULL),
(4, 'Komputer dan Aksesoris', NULL),
(5, 'Pakaian Pria', NULL),
(6, 'Pakaian Wanita', NULL),
(7, 'Jam Tangan', NULL),
(8, 'Perawatan dan Kecantikan', NULL),
(9, 'Perlengkapan Rumah', NULL),
(10, 'Sepatu Pria', NULL),
(11, 'Sepatu Wanita', NULL),
(12, 'Tas Pria', NULL),
(13, 'Tas Wanita', NULL),
(14, 'Makanan dan Minuman', NULL),
(15, 'Kesehatan', NULL),
(16, 'Olahraga dan Outdoor', NULL),
(17, 'Otomotif', NULL),
(18, 'Buku dan Alat Tulis', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(30) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `description` text DEFAULT NULL,
  `users_id` int(11) UNSIGNED NOT NULL,
  `phone_number` varchar(30) DEFAULT NULL,
  `status` enum('active','suspend') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Triggers `store`
--
DELIMITER $$
CREATE TRIGGER `store_medsos_ad` AFTER DELETE ON `store` FOR EACH ROW BEGIN
  DELETE FROM medsos WHERE medsos.value = OLD.id AND medsos.name='store'; 

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `store_medsos_au` AFTER UPDATE ON `store` FOR EACH ROW BEGIN
  DELETE FROM medsos WHERE medsos.value = OLD.id AND medsos.name='store'; 

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `name`, `code`, `phone`, `address`, `description`) VALUES
(1, 'Kios Komputer', 'KK', '087725372622', 'Malang', 'Toko peralatan komputer dan gaming gadget'),
(3, 'Baru Banget', 'BB', '111111', 'Mojokerto', 'Baru description edit'),
(4, 'Supplier Baru', 'SB', '12334534', 'Bekasi', 'Supplier baru untuk testing edit');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `value` varchar(30) NOT NULL,
  `dir` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `name`, `value`, `dir`, `file`, `created_at`, `updated_at`, `deleted_at`) VALUES
(57, 'fakultas', '17', 'img/src/fakultas/', 'fas_1617889773_84182cdaf36ed0c97307.jpeg', NULL, NULL, NULL),
(61, 'struktur_unair', '1', 'img/src/struktur_unair/', 'fas_1618458900_9386b3efc2db936a0ce8.jpg', NULL, NULL, NULL),
(62, 'struktur_unair', '2', 'img/src/struktur_unair/', 'fas_1618458923_10fa3342033fb526044a.jpg', NULL, NULL, NULL),
(63, 'struktur_unair', '3', 'img/src/struktur_unair/', 'fas_1618458937_a2d5be0d303a9fca987f.jpg', NULL, NULL, NULL),
(64, 'struktur_unair', '4', 'img/src/struktur_unair/', 'fas_1618458999_ca545ac74d469971f0e6.jpg', NULL, NULL, NULL),
(65, 'struktur_unair', '5', 'img/src/struktur_unair/', 'fas_1618459016_dbfe2c7535166d16d582.jpg', NULL, NULL, NULL),
(74, 'kampus', '2', 'img/src/kampus/', 'fas_1618580464_da29fcf8fef574bd8741.jpg', '2021-04-16 20:41:04', '2021-04-16 20:41:04', NULL),
(76, 'fasilitas', '10', 'img/src/fasilitas/', 'fas_1618580976_4322b66c9c33be883e94.jpg', '2021-04-16 20:49:36', '2021-04-16 20:49:36', NULL),
(82, 'kampus', '1', 'img/src/kampus/', 'fas_1620574386_547c1fd1f269123d4278.jpg', '2021-05-09 22:33:06', '2021-05-09 22:33:06', NULL),
(83, 'kampus', '3', 'img/src/kampus/', 'fas_1620574432_38e90d6c031f19e29db3.png', '2021-05-09 22:33:52', '2021-05-09 22:33:52', NULL),
(86, 'fakultas', '2', 'img/src/fakultas/', 'fas_1620574886_b753cc36295479ae9392.jpg', '2021-05-09 22:41:26', '2021-05-09 22:41:26', NULL),
(87, 'fakultas', '1', 'img/src/fakultas/', 'fas_1620574907_f2d1b62bb7bdfde64292.jpg', '2021-05-09 22:41:47', '2021-05-09 22:41:47', NULL),
(88, 'fakultas', '3', 'img/src/fakultas/', 'fas_1620574957_951450973851da0921d3.jpg', '2021-05-09 22:42:37', '2021-05-09 22:42:37', NULL),
(89, 'fakultas', '4', 'img/src/fakultas/', 'fas_1620574997_ea444b6a2c3de1047863.jpg', '2021-05-09 22:43:17', '2021-05-09 22:43:17', NULL),
(90, 'fakultas', '5', 'img/src/fakultas/', 'fas_1620575689_897f767e9eaef889fcb2.jpg', '2021-05-09 22:54:49', '2021-05-09 22:54:49', NULL),
(91, 'fakultas', '6', 'img/src/fakultas/', 'fas_1620575833_c8f6fff800bfbad166d4.jpg', '2021-05-09 22:57:13', '2021-05-09 22:57:13', NULL),
(92, 'fakultas', '7', 'img/src/fakultas/', 'fas_1620575882_6532df4306f841484988.jpg', '2021-05-09 22:58:02', '2021-05-09 22:58:02', NULL),
(95, 'fakultas', '11', 'img/src/fakultas/', 'fas_1620576081_70205342cc60b4e1ffaf.jpg', '2021-05-09 23:01:21', '2021-05-09 23:01:21', NULL),
(96, 'fakultas', '12', 'img/src/fakultas/', 'fas_1620576122_86b7f831fbfa1093c0f6.jpg', '2021-05-09 23:02:02', '2021-05-09 23:02:02', NULL),
(97, 'fakultas', '14', 'img/src/fakultas/', 'fas_1620576531_631299dea1d2b0477c2e.jpg', '2021-05-09 23:08:52', '2021-05-09 23:08:52', NULL),
(98, 'fakultas', '15', 'img/src/fakultas/', 'fas_1620576648_fe321977bd2af452a10c.jpg', '2021-05-09 23:10:48', '2021-05-09 23:10:48', NULL),
(99, 'fakultas', '13', 'img/src/fakultas/', 'fas_1620577649_06d027c39b616226cfac.jpg', '2021-05-09 23:27:29', '2021-05-09 23:27:29', NULL),
(100, 'fakultas', '10', 'img/src/fakultas/', 'fas_1620577692_fd5c51f38d62a7c4b2a0.jpg', '2021-05-09 23:28:12', '2021-05-09 23:28:12', NULL),
(101, 'fakultas', '18', 'img/src/fakultas/', 'fas_1620577817_c5469c91e71ea82a5ad9.jpg', '2021-05-09 23:30:17', '2021-05-09 23:30:17', NULL),
(104, 'struktur_bem', '8', 'img/src/struktur_bem/', 'fas_1620703673_ae225557cf0d83745ce0.jpg', '2021-05-11 10:27:53', '2021-05-11 10:27:53', NULL),
(105, 'struktur_bem', '9', 'img/src/struktur_bem/', 'fas_1620703701_ef4a28890e12ca9f85e0.jpg', '2021-05-11 10:28:22', '2021-05-11 10:28:22', NULL),
(106, 'struktur_bem', '10', 'img/src/struktur_bem/', 'fas_1620703794_ad8819a753cd11856317.jpg', '2021-05-11 10:29:55', '2021-05-11 10:29:55', NULL),
(107, 'struktur_bem', '11', 'img/src/struktur_bem/', 'fas_1620703834_c26f2a8135c42986fa0a.jpg', '2021-05-11 10:30:35', '2021-05-11 10:30:35', NULL),
(108, 'struktur_bem', '12', 'img/src/struktur_bem/', 'fas_1620703931_2036a7fd156f7a0c9e16.jpg', '2021-05-11 10:32:11', '2021-05-11 10:32:11', NULL),
(109, 'struktur_bem', '13', 'img/src/struktur_bem/', 'fas_1620703962_2e4f732c2d41460af6d2.jpg', '2021-05-11 10:32:42', '2021-05-11 10:32:42', NULL),
(110, 'struktur_bem', '14', 'img/src/struktur_bem/', 'fas_1620703994_0496118248c0a7a57698.jpg', '2021-05-11 10:33:14', '2021-05-11 10:33:14', NULL),
(111, 'struktur_bem', '15', 'img/src/struktur_bem/', 'fas_1620704029_f9ddc5f2b0c24a3df24a.jpg', '2021-05-11 10:33:49', '2021-05-11 10:33:49', NULL),
(112, 'struktur_bem', '16', 'img/src/struktur_bem/', 'fas_1620704334_4df8fb68ce9aa3a0210b.jpg', '2021-05-11 10:38:54', '2021-05-11 10:38:54', NULL),
(113, 'struktur_bem', '17', 'img/src/struktur_bem/', 'fas_1620704364_258a4f40c10faed041f7.jpg', '2021-05-11 10:39:25', '2021-05-11 10:39:25', NULL),
(114, 'struktur_bem', '18', 'img/src/struktur_bem/', 'fas_1620704395_1310c6de09b746a29ff0.jpg', '2021-05-11 10:39:55', '2021-05-11 10:39:55', NULL),
(115, 'struktur_bem', '19', 'img/src/struktur_bem/', 'fas_1620704428_ea68e514f0eb06fe4dc0.jpg', '2021-05-11 10:40:28', '2021-05-11 10:40:28', NULL),
(116, 'struktur_bem', '20', 'img/src/struktur_bem/', 'fas_1620704475_fc7a44ec605850f116d8.jpg', '2021-05-11 10:41:15', '2021-05-11 10:41:15', NULL),
(117, 'struktur_bem', '21', 'img/src/struktur_bem/', 'fas_1620704508_7a7559c33ab340411343.jpg', '2021-05-11 10:41:48', '2021-05-11 10:41:48', NULL),
(118, 'struktur_bem', '22', 'img/src/struktur_bem/', 'fas_1620704544_e28a54817ab41d7eea77.jpg', '2021-05-11 10:42:24', '2021-05-11 10:42:24', NULL),
(119, 'struktur_bem', '23', 'img/src/struktur_bem/', 'fas_1620704577_5e4873637b169468394e.jpg', '2021-05-11 10:42:57', '2021-05-11 10:42:57', NULL),
(120, 'struktur_bem', '24', 'img/src/struktur_bem/', 'fas_1620704608_259e10867deb5c772d28.jpg', '2021-05-11 10:43:28', '2021-05-11 10:43:28', NULL),
(121, 'struktur_bem', '25', 'img/src/struktur_bem/', 'fas_1620704637_5a5fa5da9eb203987dcf.jpg', '2021-05-11 10:43:57', '2021-05-11 10:43:57', NULL),
(122, 'struktur_bem', '26', 'img/src/struktur_bem/', 'fas_1620704875_f6cd666c2c04053bf997.jpg', '2021-05-11 10:47:55', '2021-05-11 10:47:55', NULL),
(123, 'struktur_bem', '27', 'img/src/struktur_bem/', 'fas_1620704906_7b3c97c332a3496f0248.jpg', '2021-05-11 10:48:26', '2021-05-11 10:48:26', NULL),
(124, 'struktur_bem', '28', 'img/src/struktur_bem/', 'fas_1620704937_7af32d812ebbceaf4d2d.jpg', '2021-05-11 10:48:57', '2021-05-11 10:48:57', NULL),
(125, 'fasilitas', '17', 'img/src/fasilitas/', 'fas_1620713879_ec0c19fffe501a148b4c.jpeg', '2021-05-11 13:17:59', '2021-05-11 13:17:59', NULL),
(126, 'fasilitas', '19', 'img/src/fasilitas/', 'fas_1620713908_ef2b8b24cfa1e27a4f6a.jpg', '2021-05-11 13:18:28', '2021-05-11 13:18:28', NULL),
(127, 'fasilitas', '18', 'img/src/fasilitas/', 'fas_1620713970_f256310c9e0f00fc9fd9.jpg', '2021-05-11 13:19:30', '2021-05-11 13:19:30', NULL),
(128, 'fasilitas', '20', 'img/src/fasilitas/', 'fas_1620714024_2beeb765988e2775d93d.jpg', '2021-05-11 13:20:24', '2021-05-11 13:20:24', NULL),
(131, 'fasilitas', '4', 'img/src/fasilitas/', 'fas_1620714595_88af6bedb4317a0597cd.jpg', '2021-05-11 13:29:55', '2021-05-11 13:29:55', NULL),
(132, 'fasilitas', '7', 'img/src/fasilitas/', 'fas_1620714919_adc8ca53f5e5396912ac.jpg', '2021-05-11 13:35:19', '2021-05-11 13:35:19', NULL),
(133, 'fasilitas', '3', 'img/src/fasilitas/', 'fas_1620716002_282f306692940af332e6.jpg', '2021-05-11 13:53:22', '2021-05-11 13:53:22', NULL),
(136, 'fasilitas', '5', 'img/src/fasilitas/', 'fas_1620749204_a88ac77673b40526cf3c.jpg', '2021-05-11 23:06:44', '2021-05-11 23:06:44', NULL),
(139, 'struktur_bem', '7', 'img/src/struktur_bem/', 'fas_1620762978_f48fbd49c93eca353b67.jpg', '2021-05-12 02:56:18', '2021-05-12 02:56:18', NULL),
(153, 'struktur_bem', '6', 'img/src/struktur_bem/', 'fas_1620774500_62641abee8227615ff48.jpg', '2021-05-12 06:08:21', '2021-05-12 06:08:21', NULL),
(154, 'fasilitas', '2', 'img/src/fasilitas/', 'fas_1621063495_cfe26e25fe0d94a1ea98.jpg', '2021-05-15 14:24:55', '2021-05-15 14:24:55', NULL),
(155, 'fasilitas', '1', 'img/src/fasilitas/', 'fas_1621064384_460d07e1a748f2301d2a.jpg', '2021-05-15 14:39:44', '2021-05-15 14:39:44', NULL),
(156, 'fasilitas', '13', 'img/src/fasilitas/', 'fas_1621064709_d83dd0adc9e33b3e5508.jpg', '2021-05-15 14:45:09', '2021-05-15 14:45:09', NULL),
(159, 'fasilitas', '9', 'img/src/fasilitas/', 'fas_1621065318_1e2d18e7847f3a6181e2.jpg', '2021-05-15 14:55:18', '2021-05-15 14:55:18', NULL),
(160, 'fasilitas', '12', 'img/src/fasilitas/', 'fas_1621065616_4da358f0530fb6646aa3.jpg', '2021-05-15 15:00:16', '2021-05-15 15:00:16', NULL),
(163, 'fasilitas', '29', 'img/src/fasilitas/', 'fas_1621065976_400f97b13f83eef17658.jpg', '2021-05-15 15:06:16', '2021-05-15 15:06:16', NULL),
(165, 'fasilitas', '16', 'img/src/fasilitas/', 'fas_1621066125_37996093650ae5225ffa.jpg', '2021-05-15 15:08:45', '2021-05-15 15:08:45', NULL),
(166, 'fasilitas', '14', 'img/src/fasilitas/', 'fas_1621066296_2a2c8b0ede93a1a5bbc1.jpeg', '2021-05-15 15:11:36', '2021-05-15 15:11:36', NULL),
(168, 'fasilitas', '23', 'img/src/fasilitas/', 'fas_1621066618_bc79aed055c9927dda02.jpg', '2021-05-15 15:16:58', '2021-05-15 15:16:58', NULL),
(169, 'fasilitas', '25', 'img/src/fasilitas/', 'fas_1621066955_f3e4f3fcde3e32da0945.jpg', '2021-05-15 15:22:35', '2021-05-15 15:22:35', NULL),
(170, 'fasilitas', '26', 'img/src/fasilitas/', 'fas_1621066992_be24f314a8dbf61013ff.jpg', '2021-05-15 15:23:12', '2021-05-15 15:23:12', NULL),
(171, 'fasilitas', '24', 'img/src/fasilitas/', 'fas_1621067211_44f29424504bb13dc12f.jpg', '2021-05-15 15:26:51', '2021-05-15 15:26:51', NULL),
(172, 'fasilitas', '27', 'img/src/fasilitas/', 'fas_1621067313_cdb932a5aca0fab07a50.jpg', '2021-05-15 15:28:33', '2021-05-15 15:28:33', NULL),
(173, 'fasilitas', '28', 'img/src/fasilitas/', 'fas_1621067374_f83c114c5e0fc435664a.jpg', '2021-05-15 15:29:34', '2021-05-15 15:29:34', NULL),
(175, 'fasilitas', '32', 'img/src/fasilitas/', 'fas_1621067512_8bab130fd6f340e888ea.jpg', '2021-05-15 15:31:52', '2021-05-15 15:31:52', NULL),
(176, 'fasilitas', '22', 'img/src/fasilitas/', 'fas_1621067555_10371cf5fc3669c6c18a.jpg', '2021-05-15 15:32:35', '2021-05-15 15:32:35', NULL),
(177, 'fasilitas', '21', 'img/src/fasilitas/', 'fas_1621067620_40a649550a9c922bf2a7.jpg', '2021-05-15 15:33:40', '2021-05-15 15:33:40', NULL),
(178, 'fasilitas', '8', 'img/src/fasilitas/', 'fas_1621067662_663dfe8162696975b1ce.jpg', '2021-05-15 15:34:22', '2021-05-15 15:34:22', NULL),
(179, 'fasilitas', '31', 'img/src/fasilitas/', 'fas_1621067820_6cb6d1b8b9b05196a639.jpg', '2021-05-15 15:37:00', '2021-05-15 15:37:00', NULL),
(180, 'fasilitas', '30', 'img/src/fasilitas/', 'fas_1621067849_3752a8336d71fb1e6891.jpg', '2021-05-15 15:37:29', '2021-05-15 15:37:29', NULL),
(182, 'ormawa', '1', 'img/src/ormawa/', 'fas_1621592103_71fe94aed8bd17826900.png', '2021-05-21 17:15:03', '2021-05-21 17:15:03', NULL),
(186, 'ormawa', '9', 'img/src/ormawa/', 'fas_1621933063_7afc55afb77cb4bd6acd.png', '2021-05-25 15:57:44', '2021-05-25 15:57:44', NULL),
(187, 'ormawa', '13', 'img/src/ormawa/', 'fas_1621933852_7a432926afc526e6c219.png', '2021-05-25 16:10:52', '2021-05-25 16:10:52', NULL),
(188, 'ormawa', '9', 'img/src/ormawa/', 'fas_1621934863_9f90916f058203310683.jpeg', '2021-05-25 16:27:43', '2021-05-25 16:27:43', NULL),
(189, 'ormawa', '4', 'img/src/ormawa/', 'fas_1621935329_48ebea69713897731fbb.png', '2021-05-25 16:35:29', '2021-05-25 16:35:29', NULL),
(190, 'ormawa', '16', 'img/src/ormawa/', 'fas_1621936096_0bfd0ec559979ed4e242.jpg', '2021-05-25 16:48:16', '2021-05-25 16:48:16', NULL),
(191, 'ormawa', '18', 'img/src/ormawa/', 'fas_1621939133_9dd867fc3d6ec9cf5b01.png', '2021-05-25 17:38:53', '2021-05-25 17:38:53', NULL),
(192, 'ormawa', '19', 'img/src/ormawa/', 'fas_1621939747_8eda2c3aff6cd0726f4d.png', '2021-05-25 17:49:07', '2021-05-25 17:49:07', NULL),
(193, 'ormawa', '20', 'img/src/ormawa/', 'fas_1621940090_adbb55aaf4da19243d0e.png', '2021-05-25 17:54:51', '2021-05-25 17:54:51', NULL),
(194, 'ormawa', '21', 'img/src/ormawa/', 'fas_1621940619_7b117bb5d92f676c5e0c.png', '2021-05-25 18:03:40', '2021-05-25 18:03:40', NULL),
(195, 'ormawa', '22', 'img/src/ormawa/', 'fas_1621941257_901d17c55585167dd03a.png', '2021-05-25 18:14:18', '2021-05-25 18:14:18', NULL),
(196, 'ormawa', '24', 'img/src/ormawa/', 'fas_1621943254_77e58dbb20da7c59a049.png', '2021-05-25 18:47:34', '2021-05-25 18:47:34', NULL),
(197, 'ormawa', '14', 'img/src/ormawa/', 'fas_1621943622_f530e8d3704b6bbe9382.png', '2021-05-25 18:53:42', '2021-05-25 18:53:42', NULL),
(198, 'ormawa', '8', 'img/src/ormawa/', 'fas_1621943623_0f61bd98574e1a0c877d.png', '2021-05-25 18:53:43', '2021-05-25 18:53:43', NULL),
(199, 'ormawa', '25', 'img/src/ormawa/', 'fas_1621943837_21ce7dcb901f68cc538e.png', '2021-05-25 18:57:17', '2021-05-25 18:57:17', NULL),
(200, 'ormawa', '26', 'img/src/ormawa/', 'fas_1621944010_a1eba0554cb14421ffce.png', '2021-05-25 19:00:11', '2021-05-25 19:00:11', NULL),
(201, 'ormawa', '27', 'img/src/ormawa/', 'fas_1621944255_612b9b2d6d94c3293a47.jpeg', '2021-05-25 19:04:15', '2021-05-25 19:04:15', NULL),
(202, 'ormawa', '28', 'img/src/ormawa/', 'fas_1621945458_64c2fe0d2e2eda27ad2d.png', '2021-05-25 19:24:19', '2021-05-25 19:24:19', NULL),
(203, 'ormawa', '14', 'img/src/ormawa/', 'fas_1621947667_ebbab7321bfdbb9187b8.png', '2021-05-25 20:01:07', '2021-05-25 20:01:07', NULL),
(204, 'news', '6', 'img/src/news/', 'fas_1621950895_9988296e295ef7074b1e.png', '2021-05-25 20:54:55', '2021-05-25 20:54:55', NULL),
(205, 'ormawa', '72', 'img/src/ormawa/', 'fas_1622035342_a36ab1be54beee9d0f8d.png', '2021-05-26 20:22:22', '2021-05-26 20:22:22', NULL),
(206, 'ormawa', '17', 'img/src/ormawa/', 'fas_1622391623_f6228023e95604de2f30.png', '2021-05-30 23:20:23', '2021-05-30 23:20:23', NULL),
(207, 'ormawa', '73', 'img/src/ormawa/', 'fas_1622392374_69b97c5bffff3a290b83.jpg', '2021-05-30 23:32:54', '2021-05-30 23:32:54', NULL),
(209, 'news', '14', 'img/src/news/', 'fas_1622394587_2643e891b6110448d0a3.png', '2021-05-31 00:09:48', '2021-05-31 00:09:48', NULL),
(211, 'news', '16', 'img/src/news/', 'fas_1622395489_31193be75791bd4bcb5f.png', '2021-05-31 00:24:49', '2021-05-31 00:24:49', NULL),
(216, 'news', '15', 'img/src/news/', 'fas_1622438693_87b14ca2b001619d2d64.png', '2021-05-31 12:24:54', '2021-05-31 12:24:54', NULL),
(217, 'news', '13', 'img/src/news/', 'fas_1622438734_ae8f996407c7b81ebbb6.jpg', '2021-05-31 12:25:34', '2021-05-31 12:25:34', NULL),
(218, 'ormawa', '29', 'img/src/ormawa/', 'fas_1622549020_973993cfb044d6bbb423.jpg', '2021-06-01 19:03:40', '2021-06-01 19:03:40', NULL),
(219, 'ormawa', '30', 'img/src/ormawa/', 'fas_1622549073_67914824f9262f33bd85.png', '2021-06-01 19:04:33', '2021-06-01 19:04:33', NULL),
(220, 'ormawa', '31', 'img/src/ormawa/', 'fas_1622549106_951cdad9861971aca960.png', '2021-06-01 19:05:06', '2021-06-01 19:05:06', NULL),
(221, 'ormawa', '32', 'img/src/ormawa/', 'fas_1622549405_7dd2720666586ba0df4a.png', '2021-06-01 19:10:05', '2021-06-01 19:10:05', NULL),
(222, 'ormawa', '33', 'img/src/ormawa/', 'fas_1622549442_7394ed14229c4c887bbd.png', '2021-06-01 19:10:42', '2021-06-01 19:10:42', NULL),
(223, 'ormawa', '34', 'img/src/ormawa/', 'fas_1622549471_f83f12cb34a26708a30c.jpg', '2021-06-01 19:11:11', '2021-06-01 19:11:11', NULL),
(224, 'ormawa', '35', 'img/src/ormawa/', 'fas_1622549498_7b401c9c299827d0efff.png', '2021-06-01 19:11:38', '2021-06-01 19:11:38', NULL),
(225, 'ormawa', '36', 'img/src/ormawa/', 'fas_1622549770_6cbcf5e173c6dbb843bf.jpg', '2021-06-01 19:16:10', '2021-06-01 19:16:10', NULL),
(226, 'ormawa', '37', 'img/src/ormawa/', 'fas_1622549799_88a04fc4aea3483b9091.png', '2021-06-01 19:16:39', '2021-06-01 19:16:39', NULL),
(227, 'ormawa', '38', 'img/src/ormawa/', 'fas_1622549840_8266d897b22919a89273.png', '2021-06-01 19:17:20', '2021-06-01 19:17:20', NULL),
(228, 'ormawa', '39', 'img/src/ormawa/', 'fas_1622549895_891ae7d9d696ff7fe19f.png', '2021-06-01 19:18:16', '2021-06-01 19:18:16', NULL),
(229, 'ormawa', '40', 'img/src/ormawa/', 'fas_1622549991_6a95d122425a677d3c17.jpg', '2021-06-01 19:19:51', '2021-06-01 19:19:51', NULL),
(230, 'ormawa', '41', 'img/src/ormawa/', 'fas_1622550020_c6720e986e5ce81a39db.png', '2021-06-01 19:20:20', '2021-06-01 19:20:20', NULL),
(231, 'ormawa', '42', 'img/src/ormawa/', 'fas_1622550051_e21f49aea1404ec749d2.png', '2021-06-01 19:20:52', '2021-06-01 19:20:52', NULL),
(232, 'ormawa', '43', 'img/src/ormawa/', 'fas_1622550097_cc8b7bb31528467ea532.png', '2021-06-01 19:21:37', '2021-06-01 19:21:37', NULL),
(233, 'ormawa', '44', 'img/src/ormawa/', 'fas_1622550118_7e535a6680728b529a58.png', '2021-06-01 19:21:58', '2021-06-01 19:21:58', NULL),
(234, 'ormawa', '45', 'img/src/ormawa/', 'fas_1622550147_9aca3cb1564d5088ab06.png', '2021-06-01 19:22:28', '2021-06-01 19:22:28', NULL),
(235, 'ormawa', '46', 'img/src/ormawa/', 'fas_1622550170_95b3f2e66fd187b60f92.png', '2021-06-01 19:22:50', '2021-06-01 19:22:50', NULL),
(236, 'ormawa', '47', 'img/src/ormawa/', 'fas_1622550197_a0debee53cce503ea529.png', '2021-06-01 19:23:18', '2021-06-01 19:23:18', NULL),
(237, 'ormawa', '48', 'img/src/ormawa/', 'fas_1622550251_8a2ff7cc9a5c22bc75bd.png', '2021-06-01 19:24:11', '2021-06-01 19:24:11', NULL),
(238, 'ormawa', '49', 'img/src/ormawa/', 'fas_1622550283_1c7e8deda353c6f51af5.png', '2021-06-01 19:24:43', '2021-06-01 19:24:43', NULL),
(239, 'ormawa', '50', 'img/src/ormawa/', 'fas_1622550394_b0f1f1f03849127600bc.png', '2021-06-01 19:26:35', '2021-06-01 19:26:35', NULL),
(240, 'ormawa', '51', 'img/src/ormawa/', 'fas_1622550487_183701d65c564dccf0b2.png', '2021-06-01 19:28:08', '2021-06-01 19:28:08', NULL),
(241, 'ormawa', '52', 'img/src/ormawa/', 'fas_1622550512_efc5403bcb603e108f6d.png', '2021-06-01 19:28:32', '2021-06-01 19:28:32', NULL),
(242, 'ormawa', '53', 'img/src/ormawa/', 'fas_1622550566_68095c406cf953ab18f8.png', '2021-06-01 19:29:26', '2021-06-01 19:29:26', NULL),
(243, 'ormawa', '54', 'img/src/ormawa/', 'fas_1622550605_9b5d9f12873a54817e27.png', '2021-06-01 19:30:05', '2021-06-01 19:30:05', NULL),
(244, 'ormawa', '55', 'img/src/ormawa/', 'fas_1622550660_19868a7c2cc27e67af5f.png', '2021-06-01 19:31:00', '2021-06-01 19:31:00', NULL),
(245, 'ormawa', '56', 'img/src/ormawa/', 'fas_1622550694_d4c9381bdc19720297e3.png', '2021-06-01 19:31:34', '2021-06-01 19:31:34', NULL),
(246, 'ormawa', '57', 'img/src/ormawa/', 'fas_1622550759_51e9098310221502d1b8.png', '2021-06-01 19:32:41', '2021-06-01 19:32:41', NULL),
(247, 'ormawa', '58', 'img/src/ormawa/', 'fas_1622550815_061dbb53ff7d2a36e32f.png', '2021-06-01 19:33:35', '2021-06-01 19:33:35', NULL),
(248, 'ormawa', '60', 'img/src/ormawa/', 'fas_1622550899_7ff0b64e1f67e59c0919.png', '2021-06-01 19:34:59', '2021-06-01 19:34:59', NULL),
(249, 'ormawa', '59', 'img/src/ormawa/', 'fas_1622550956_0d6578a24fe094524227.png', '2021-06-01 19:35:56', '2021-06-01 19:35:56', NULL),
(250, 'ormawa', '61', 'img/src/ormawa/', 'fas_1622551016_cd14484a5eb518a0fb0e.png', '2021-06-01 19:36:56', '2021-06-01 19:36:56', NULL),
(251, 'ormawa', '62', 'img/src/ormawa/', 'fas_1622551051_b8edb1e65fa2f47810f2.jpg', '2021-06-01 19:37:31', '2021-06-01 19:37:31', NULL),
(252, 'ormawa', '63', 'img/src/ormawa/', 'fas_1622551147_aff4c23ebddbad9ccd61.png', '2021-06-01 19:39:07', '2021-06-01 19:39:07', NULL),
(253, 'ormawa', '64', 'img/src/ormawa/', 'fas_1622551195_a7c16bb6dfa0ae4f873a.png', '2021-06-01 19:39:56', '2021-06-01 19:39:56', NULL),
(254, 'ormawa', '65', 'img/src/ormawa/', 'fas_1622551719_4eceeb3a25a990d9090d.jpeg', '2021-06-01 19:48:39', '2021-06-01 19:48:39', NULL),
(255, 'ormawa', '66', 'img/src/ormawa/', 'fas_1622551810_20981eddd517d155bf62.png', '2021-06-01 19:50:12', '2021-06-01 19:50:12', NULL),
(256, 'ormawa', '67', 'img/src/ormawa/', 'fas_1622551866_e6eaae31649cd596156e.png', '2021-06-01 19:51:06', '2021-06-01 19:51:06', NULL),
(257, 'ormawa', '68', 'img/src/ormawa/', 'fas_1622551964_0e3340bb58d88b2b52d6.png', '2021-06-01 19:52:45', '2021-06-01 19:52:45', NULL),
(258, 'ormawa', '69', 'img/src/ormawa/', 'fas_1622552046_b93834e9d5e61f73e854.png', '2021-06-01 19:54:06', '2021-06-01 19:54:06', NULL),
(259, 'ormawa', '70', 'img/src/ormawa/', 'fas_1622552249_5750e58eaa583d88228e.jpg', '2021-06-01 19:57:29', '2021-06-01 19:57:29', NULL),
(260, 'ormawa', '71', 'img/src/ormawa/', 'fas_1622552289_6d38ee28df16c2acc230.png', '2021-06-01 19:58:09', '2021-06-01 19:58:09', NULL),
(265, 'fakultas', '9', 'img/src/fakultas/', 'fas_1622622519_b04df54a1d1327405e2f.jpeg', '2021-06-02 15:28:39', '2021-06-02 15:28:39', NULL),
(266, 'fakultas', '8', 'img/src/fakultas/', 'fas_1622622571_ed057b88376952cb8bd9.jpeg', '2021-06-02 15:29:31', '2021-06-02 15:29:31', NULL),
(267, 'ormawa', '75', 'img/src/ormawa/', 'fas_1622623410_3b9173ef2ea45401c59d.png', '2021-06-02 15:43:30', '2021-06-02 15:43:30', NULL),
(268, 'ormawa', '76', 'img/src/ormawa/', 'fas_1622624155_50db0e1f7cf05d2766f0.png', '2021-06-02 15:55:55', '2021-06-02 15:55:55', NULL),
(269, 'ormawa', '77', 'img/src/ormawa/', 'fas_1622624968_725f6b3e453203689d0e.png', '2021-06-02 16:09:28', '2021-06-02 16:09:28', NULL),
(270, 'ormawa', '78', 'img/src/ormawa/', 'fas_1622626419_9cbe3b1954630ee0fb8d.jpg', '2021-06-02 16:33:39', '2021-06-02 16:33:39', NULL),
(350, 'news', '20', 'img/src/news/', 'fas_1626973902_e91c2ec9bcb234f07a2c.png', '2021-07-23 00:11:43', '2021-07-23 00:11:43', NULL),
(351, 'news', '21', 'img/src/news/', 'fas_1626974281_66399699aeadae78cccb.png', '2021-07-23 00:18:01', '2021-07-23 00:18:01', NULL),
(352, 'news', '22', 'img/src/news/', 'fas_1626974402_cf6eb96da2c224c212e5.jpg', '2021-07-23 00:20:02', '2021-07-23 00:20:02', NULL),
(353, 'news', '23', 'img/src/news/', 'fas_1626974629_8a32ac9015d3b58485cd.jpg', '2021-07-23 00:23:49', '2021-07-23 00:23:49', NULL),
(354, 'news', '24', 'img/src/news/', 'fas_1626974794_3685e9b4347033f1e52d.jpg', '2021-07-23 00:26:35', '2021-07-23 00:26:35', NULL),
(355, 'news', '25', 'img/src/news/', 'fas_1626974939_e979c468d99faa15f3ad.png', '2021-07-23 00:29:00', '2021-07-23 00:29:00', NULL),
(356, 'news', '26', 'img/src/news/', 'fas_1626975064_0fe1d3d7f5175596bb10.jpg', '2021-07-23 00:31:04', '2021-07-23 00:31:04', NULL),
(357, 'news', '27', 'img/src/news/', 'fas_1626975189_5dcbb01c35398acb17d2.png', '2021-07-23 00:33:09', '2021-07-23 00:33:09', NULL),
(358, 'news', '28', 'img/src/news/', 'fas_1626975415_3799785b43026c326348.jpeg', '2021-07-23 00:36:55', '2021-07-23 00:36:55', NULL),
(359, 'news', '29', 'img/src/news/', 'fas_1626975561_323b8373b69e27e94b9f.jpeg', '2021-07-23 00:39:21', '2021-07-23 00:39:21', NULL),
(360, 'news', '30', 'img/src/news/', 'fas_1626975700_921661858f728d5ed12f.png', '2021-07-23 00:41:41', '2021-07-23 00:41:41', NULL),
(361, 'news', '31', 'img/src/news/', 'fas_1626975784_1cd91707203aa9ec0287.jpg', '2021-07-23 00:43:04', '2021-07-23 00:43:04', NULL),
(362, 'news', '32', 'img/src/news/', 'fas_1626975883_1870d0a2259c90f7c9fc.png', '2021-07-23 00:44:44', '2021-07-23 00:44:44', NULL),
(363, 'news', '33', 'img/src/news/', 'fas_1626976083_2f0fb45d04615b85a4d4.jpeg', '2021-07-23 00:48:03', '2021-07-23 00:48:03', NULL),
(364, 'news', '34', 'img/src/news/', 'fas_1626976177_de3a3bb7a1e13f409f9b.jpg', '2021-07-23 00:49:37', '2021-07-23 00:49:37', NULL),
(365, 'news', '35', 'img/src/news/', 'fas_1626976269_cc38c7fa3c812ba0198c.jpg', '2021-07-23 00:51:10', '2021-07-23 00:51:10', NULL),
(366, 'news', '36', 'img/src/news/', 'fas_1626976348_5e0a5cc944236c777705.png', '2021-07-23 00:52:28', '2021-07-23 00:52:28', NULL),
(367, 'news', '37', 'img/src/news/', 'fas_1626976515_386dfeafa6c5ed8a1b29.jpeg', '2021-07-23 00:55:15', '2021-07-23 00:55:15', NULL),
(368, 'news', '38', 'img/src/news/', 'fas_1626976650_a404cb3f116d4a67987a.jpeg', '2021-07-23 00:57:30', '2021-07-23 00:57:30', NULL),
(369, 'news', '39', 'img/src/news/', 'fas_1626976734_e03e9a4c2ede1a42572e.jpg', '2021-07-23 00:58:55', '2021-07-23 00:58:55', NULL),
(370, 'news', '40', 'img/src/news/', 'fas_1627139538_adbc9365aa277a0cc3fb.png', '2021-07-24 22:12:18', '2021-07-24 22:12:18', NULL),
(371, 'news', '41', 'img/src/news/', 'fas_1627139659_e5b81ec1c9b2b11716ba.jpg', '2021-07-24 22:14:19', '2021-07-24 22:14:19', NULL),
(372, 'news', '42', 'img/src/news/', 'fas_1627139881_c803a8c081e3fcade20c.png', '2021-07-24 22:18:02', '2021-07-24 22:18:02', NULL),
(373, 'produk', '57', 'img/src/produk/', 'sample.png', '2021-08-03 19:46:20', '2021-08-03 19:46:20', NULL),
(374, 'produk', '58', 'img/src/produk/', 'sample.png', '2021-08-03 19:46:20', '2021-08-03 19:46:20', NULL),
(375, 'store', '39', 'img/src/store/', 'fas_1627994780_fdabe1168585a5e170b3.jpg', '2021-08-03 19:46:20', '2021-08-03 19:46:20', NULL),
(376, 'news', '43', 'img/src/news/', 'fas_1628231466_a53d5c998cc9b8876f1b.png', '2021-08-06 13:31:07', '2021-08-06 13:31:07', NULL),
(377, 'news', '44', 'img/src/news/', 'fas_1628231666_22a24dd1751e9f198a2d.png', '2021-08-06 13:34:27', '2021-08-06 13:34:27', NULL),
(378, 'news', '45', 'img/src/news/', 'fas_1628232119_1e968cb0ac209d7d5930.jpg', '2021-08-06 13:41:59', '2021-08-06 13:41:59', NULL),
(379, 'news', '46', 'img/src/news/', 'fas_1628232221_2f50b7ad029a3f5b75f6.jpg', '2021-08-06 13:43:41', '2021-08-06 13:43:41', NULL),
(380, 'news', '47', 'img/src/news/', 'fas_1628232327_5a2a19ee49f2795391d2.jpg', '2021-08-06 13:45:27', '2021-08-06 13:45:27', NULL),
(381, 'news', '48', 'img/src/news/', 'fas_1628232458_a23f9a048f2c97b3a8da.png', '2021-08-06 13:47:39', '2021-08-06 13:47:39', NULL),
(382, 'news', '49', 'img/src/news/', 'fas_1628232581_80ced68fd0e5899af893.jpg', '2021-08-06 13:49:41', '2021-08-06 13:49:41', NULL),
(383, 'news', '50', 'img/src/news/', 'fas_1628232726_97915d69ce0f5187caf3.jpg', '2021-08-06 13:52:06', '2021-08-06 13:52:06', NULL),
(384, 'news', '51', 'img/src/news/', 'fas_1628232785_2d87a3d2f32b541e62b7.jpg', '2021-08-06 13:53:06', '2021-08-06 13:53:06', NULL),
(385, 'news', '52', 'img/src/news/', 'fas_1628232827_4e3a5b8caa5388dce7a0.jpg', '2021-08-06 13:53:47', '2021-08-06 13:53:47', NULL),
(386, 'news', '53', 'img/src/news/', 'fas_1628232922_b3cd572df05bbc0153fb.jpg', '2021-08-06 13:55:22', '2021-08-06 13:55:22', NULL),
(387, 'news', '54', 'img/src/news/', 'fas_1628232969_c513c2d569fb51c0db86.jpg', '2021-08-06 13:56:09', '2021-08-06 13:56:09', NULL),
(388, 'news', '55', 'img/src/news/', 'fas_1628233161_e89443553e189f368a78.jpg', '2021-08-06 13:59:21', '2021-08-06 13:59:21', NULL),
(389, 'news', '56', 'img/src/news/', 'fas_1628233239_1219a2382080cb761957.png', '2021-08-06 14:00:40', '2021-08-06 14:00:40', NULL),
(390, 'news', '57', 'img/src/news/', 'fas_1628233316_4f1836dd929cb597d2c5.png', '2021-08-06 14:01:57', '2021-08-06 14:01:57', NULL),
(391, 'news', '58', 'img/src/news/', 'fas_1628233401_2c1e203461b735367db0.png', '2021-08-06 14:03:22', '2021-08-06 14:03:22', NULL),
(392, 'news', '59', 'img/src/news/', 'fas_1628233555_1bf2a9ff8ac65bab0233.png', '2021-08-06 14:05:55', '2021-08-06 14:05:55', NULL),
(393, 'news', '60', 'img/src/news/', 'fas_1628233672_5e6356b1c4b148a5ff0e.jpg', '2021-08-06 14:07:52', '2021-08-06 14:07:52', NULL),
(394, 'news', '61', 'img/src/news/', 'fas_1628234093_a0a1a30f22c2cab86218.png', '2021-08-06 14:14:54', '2021-08-06 14:14:54', NULL),
(395, 'news', '62', 'img/src/news/', 'fas_1628234499_b1f9891ab69032a743f5.png', '2021-08-06 14:21:41', '2021-08-06 14:21:41', NULL),
(396, 'news', '63', 'img/src/news/', 'fas_1628328311_6b8c6d03dd859f8c7f96.png', '2021-08-07 16:25:13', '2021-08-07 16:25:13', NULL),
(397, 'news', '64', 'img/src/news/', 'fas_1628328402_7035084b328774162141.png', '2021-08-07 16:26:44', '2021-08-07 16:26:44', NULL),
(398, 'news', '65', 'img/src/news/', 'fas_1628328503_62c149782bb6ec1ef3c7.png', '2021-08-07 16:28:24', '2021-08-07 16:28:24', NULL),
(399, 'news', '66', 'img/src/news/', 'fas_1628328776_50ce12a529cefe4b4685.jpg', '2021-08-07 16:32:57', '2021-08-07 16:32:57', NULL),
(400, 'news', '67', 'img/src/news/', 'fas_1628745272_f42a132742a67ce5f048.png', '2021-08-12 12:14:33', '2021-08-12 12:14:33', NULL),
(401, 'news', '68', 'img/src/news/', 'fas_1628745352_7ed8cf16e58bc3c6c238.jpg', '2021-08-12 12:15:52', '2021-08-12 12:15:52', NULL),
(402, 'news', '69', 'img/src/news/', 'fas_1628745472_563bafae8b0a4ea110b3.jpg', '2021-08-12 12:17:52', '2021-08-12 12:17:52', NULL),
(403, 'news', '70', 'img/src/news/', 'fas_1628745673_f5a097cd99ae3bdea900.jpg', '2021-08-12 12:21:13', '2021-08-12 12:21:13', NULL),
(404, 'news', '71', 'img/src/news/', 'fas_1628746153_5c27a2bf5620c39ee935.png', '2021-08-12 12:29:14', '2021-08-12 12:29:14', NULL),
(405, 'news', '72', 'img/src/news/', 'fas_1628746235_4da706e87469de743d49.png', '2021-08-12 12:30:36', '2021-08-12 12:30:36', NULL),
(406, 'news', '73', 'img/src/news/', 'fas_1628746347_3478e9ab6157c507da13.png', '2021-08-12 12:32:28', '2021-08-12 12:32:28', NULL),
(407, 'news', '74', 'img/src/news/', 'fas_1628746478_87782be0a578e4c6ac3c.jpeg', '2021-08-12 12:34:38', '2021-08-12 12:34:38', NULL),
(408, 'produk', '59', 'img/src/produk/', 'fas_1628952097_486be5924b527ff60d7e.png', '2021-08-14 21:41:38', '2021-08-14 21:41:38', NULL),
(409, 'produk', '60', 'img/src/produk/', 'fas_1628952098_09bfecabbaaca2603883.png', '2021-08-14 21:41:38', '2021-08-14 21:41:38', NULL),
(410, 'produk', '61', 'img/src/produk/', 'fas_1628952098_a0115da85037aec00460.png', '2021-08-14 21:41:39', '2021-08-14 21:41:39', NULL),
(411, 'produk', '62', 'img/src/produk/', 'fas_1628952099_321d86eb49547bb88edd.png', '2021-08-14 21:41:39', '2021-08-14 21:41:39', NULL),
(412, 'produk', '63', 'img/src/produk/', 'sample.png', '2021-08-14 21:41:39', '2021-08-14 21:41:39', NULL),
(413, 'store', '40', 'img/src/store/', 'fas_1628952099_1b1a53a093726339a7fb.png', '2021-08-14 21:41:39', '2021-08-14 21:41:39', NULL),
(414, 'produk', '64', 'img/src/produk/', 'fas_1628954615_801ad38631c2794f0ead.jpeg', '2021-08-14 22:23:35', '2021-08-14 22:23:35', NULL),
(415, 'produk', '65', 'img/src/produk/', 'fas_1628954615_984d08ac62ebdb691633.jpeg', '2021-08-14 22:23:35', '2021-08-14 22:23:35', NULL),
(416, 'produk', '66', 'img/src/produk/', 'fas_1628954615_5f6202f346a77ff1fddb.jpeg', '2021-08-14 22:23:35', '2021-08-14 22:23:35', NULL),
(417, 'produk', '67', 'img/src/produk/', 'fas_1628954615_e2ba841d9030d1998056.jpeg', '2021-08-14 22:23:35', '2021-08-14 22:23:35', NULL),
(418, 'produk', '68', 'img/src/produk/', 'fas_1628954615_b6f039b1b0d2b059d78c.jpeg', '2021-08-14 22:23:35', '2021-08-14 22:23:35', NULL),
(419, 'store', '41', 'img/src/store/', 'fas_1628954615_562923731c0f148211f8.png', '2021-08-14 22:23:35', '2021-08-14 22:23:35', NULL),
(420, 'produk', '69', 'img/src/produk/', 'fas_1628989734_c969216d2dd03d1c4efa.jpg', '2021-08-15 08:08:54', '2021-08-15 08:08:54', NULL),
(421, 'store', '42', 'img/src/store/', 'fas_1628989734_20711d95b7f463b12efc.jpg', '2021-08-15 08:08:54', '2021-08-15 08:08:54', NULL),
(423, 'store', '60', 'img/src/store/', 'fas_1629009769_ba6f569d70a8fba6fcf1.png', '2021-08-15 13:32:05', '2021-08-15 13:42:49', NULL),
(424, 'produk', '71', 'img/src/produk/', 'fas_1629009232_21684267d8413aad738c.jpg', '2021-08-15 13:33:52', '2021-08-15 13:33:52', NULL),
(425, 'store', '61', 'img/src/store/', 'fas_1629009595_d811494b03e1d2367399.png', '2021-08-15 13:38:11', '2021-08-15 13:39:56', NULL),
(426, 'produk', '72', 'img/src/produk/', 'fas_1629009674_eb3f72e6017d58f778e7.jpg', '2021-08-15 13:41:14', '2021-08-15 13:41:14', NULL),
(427, 'produk', '73', 'img/src/produk/', 'fas_1629010167_cb7b59bdeb77e79a59e7.jpg', '2021-08-15 13:49:27', '2021-08-15 13:49:27', NULL),
(428, 'produk', '73', 'img/src/produk/', 'fas_1629010167_ee69bffd05b2a3bf329b.png', '2021-08-15 13:49:27', '2021-08-15 13:49:27', NULL),
(429, 'store', '62', 'img/src/store/', 'fas_1629013543_8b97e97e703613195e44.png', '2021-08-15 13:49:27', '2021-08-15 14:45:43', NULL),
(436, 'produk', '76', 'img/src/produk/', 'fas_1629011342_1e0c36ebacc00733dcf4.jpeg', '2021-08-15 14:09:02', '2021-08-15 14:09:02', NULL),
(437, 'store', '68', 'img/src/store/', 'fas_1629011342_9734b9cd9c4644f3d3fe.jpeg', '2021-08-15 14:09:02', '2021-08-15 14:09:02', NULL),
(438, 'produk', '77', 'img/src/produk/', 'fas_1629011583_5cc87f7f8e15451a7633.jpeg', '2021-08-15 14:13:03', '2021-08-15 14:13:03', NULL),
(439, 'produk', '78', 'img/src/produk/', 'fas_1629011905_653f3c5e2f8da3910312.png', '2021-08-15 14:18:25', '2021-08-15 14:18:25', NULL),
(440, 'produk', '79', 'img/src/produk/', 'fas_1629011905_30178377ca82d83adbe9.png', '2021-08-15 14:18:25', '2021-08-15 14:18:25', NULL),
(441, 'produk', '80', 'img/src/produk/', 'fas_1629011905_bd4410f557b1accba2f1.png', '2021-08-15 14:18:25', '2021-08-15 14:18:25', NULL),
(442, 'produk', '81', 'img/src/produk/', 'fas_1629011905_bad05adb4f84f37e71d9.png', '2021-08-15 14:18:25', '2021-08-15 14:18:25', NULL),
(443, 'store', '69', 'img/src/store/', 'fas_1629011906_4b94e99e5d5c20061c8b.png', '2021-08-15 14:18:26', '2021-08-15 14:18:26', NULL),
(444, 'produk', '82', 'img/src/produk/', 'fas_1629013351_8309c7bfa6b2dd91f051.jpeg', '2021-08-15 14:42:31', '2021-08-15 14:42:31', NULL),
(445, 'produk', '83', 'img/src/produk/', 'fas_1629013352_3619c277f2b201d60a66.jpeg', '2021-08-15 14:42:32', '2021-08-15 14:42:32', NULL),
(446, 'produk', '84', 'img/src/produk/', 'fas_1629013352_2905b41820a75a2ac2fe.jpeg', '2021-08-15 14:42:32', '2021-08-15 14:42:32', NULL),
(447, 'produk', '85', 'img/src/produk/', 'fas_1629013352_51178f43f18940507b97.jpeg', '2021-08-15 14:42:32', '2021-08-15 14:42:32', NULL),
(448, 'produk', '86', 'img/src/produk/', 'fas_1629013352_f87ac57ef71ef13cdf3d.jpeg', '2021-08-15 14:42:32', '2021-08-15 14:42:32', NULL),
(449, 'store', '70', 'img/src/store/', 'fas_1629013352_0056c305d72fccc0bd62.jpeg', '2021-08-15 14:42:32', '2021-08-15 14:42:32', NULL),
(453, 'store', '72', 'img/src/store/', 'fas_1629014014_3acfa093e00a88e936b8.jpeg', '2021-08-15 14:53:34', '2021-08-15 14:53:34', NULL),
(454, 'produk', '89', 'img/src/produk/', 'fas_1629014014_73d10ba754d6b78be303.jpeg', '2021-08-15 14:53:34', '2021-08-15 14:53:34', NULL),
(455, 'produk', '90', 'img/src/produk/', 'fas_1629014014_17d2b3f6be649116d4d4.jpeg', '2021-08-15 14:53:34', '2021-08-15 14:53:34', NULL),
(456, 'store', '73', 'img/src/store/', 'fas_1629015042_ba2568468ac3cd5b799c.jpeg', '2021-08-15 15:10:42', '2021-08-15 15:10:42', NULL),
(457, 'produk', '91', 'img/src/produk/', 'fas_1629015042_eb52691679c7a19695c8.jpeg', '2021-08-15 15:10:42', '2021-08-15 15:10:42', NULL),
(458, 'produk', '92', 'img/src/produk/', 'fas_1629015042_5667a9304f24ac73e655.jpeg', '2021-08-15 15:10:42', '2021-08-15 15:10:42', NULL),
(459, 'produk', '93', 'img/src/produk/', 'fas_1629015042_a23cd3ee47bc57c2e920.jpeg', '2021-08-15 15:10:42', '2021-08-15 15:10:42', NULL),
(460, 'produk', '94', 'img/src/produk/', 'fas_1629015042_784c42328a9982fa6c0c.jpeg', '2021-08-15 15:10:42', '2021-08-15 15:10:42', NULL),
(461, 'produk', '95', 'img/src/produk/', 'fas_1629015042_3aab1a1624e2d9e36bca.jpeg', '2021-08-15 15:10:42', '2021-08-15 15:10:42', NULL),
(462, 'store', '74', 'img/src/store/', 'fas_1629016145_7958f33ca8710e4b9e76.jpeg', '2021-08-15 15:29:05', '2021-08-15 15:29:05', NULL),
(463, 'produk', '96', 'img/src/produk/', 'fas_1629016145_0f6014809445d49ed5a9.jpeg', '2021-08-15 15:29:05', '2021-08-15 15:29:05', NULL),
(464, 'produk', '97', 'img/src/produk/', 'fas_1629016145_aaa58c19ce7d5573279a.jpeg', '2021-08-15 15:29:05', '2021-08-15 15:29:05', NULL),
(465, 'produk', '98', 'img/src/produk/', 'fas_1629016145_40021d32a98873e5d6bf.jpeg', '2021-08-15 15:29:05', '2021-08-15 15:29:05', NULL),
(466, 'produk', '99', 'img/src/produk/', 'fas_1629016145_0f07eefbb38c26ea7833.jpeg', '2021-08-15 15:29:05', '2021-08-15 15:29:05', NULL),
(467, 'store', '75', 'img/src/store/', 'fas_1629018337_5cb3b23a64159168d1d3.jpeg', '2021-08-15 16:05:37', '2021-08-15 16:05:37', NULL),
(468, 'produk', '100', 'img/src/produk/', 'sample.png', '2021-08-15 16:05:37', '2021-08-15 16:05:37', NULL),
(469, 'produk', '101', 'img/src/produk/', 'fas_1629018337_b4f6f49779a7e5d8d87c.jpg', '2021-08-15 16:05:37', '2021-08-15 16:05:37', NULL),
(470, 'produk', '100', 'img/src/produk/', 'fas_1629018442_6c08927ba3ef8a65476e.jpg', '2021-08-15 16:07:22', '2021-08-15 16:07:22', NULL),
(471, 'store', '76', 'img/src/store/', 'fas_1629019941_68e1d9f746e778633098.jpeg', '2021-08-15 16:32:21', '2021-08-15 16:32:21', NULL),
(472, 'produk', '102', 'img/src/produk/', 'fas_1629019941_bbb6dd5f3382eb75bb4c.jpeg', '2021-08-15 16:32:21', '2021-08-15 16:32:21', NULL),
(473, 'produk', '103', 'img/src/produk/', 'fas_1629019941_9561f9172bf774112694.jpeg', '2021-08-15 16:32:21', '2021-08-15 16:32:21', NULL),
(474, 'produk', '104', 'img/src/produk/', 'fas_1629019941_a0385bb2c981c1346393.jpeg', '2021-08-15 16:32:21', '2021-08-15 16:32:21', NULL),
(475, 'produk', '105', 'img/src/produk/', 'fas_1629019941_8160b1b49f91a8c5eda2.jpeg', '2021-08-15 16:32:21', '2021-08-15 16:32:21', NULL),
(476, 'produk', '106', 'img/src/produk/', 'fas_1629019941_2ab43d2880d9b46a97d7.jpeg', '2021-08-15 16:32:21', '2021-08-15 16:32:21', NULL),
(477, 'store', '77', 'img/src/store/', 'fas_1629020642_49b6b49f3382a9e044e2.jpeg', '2021-08-15 16:44:02', '2021-08-15 16:44:02', NULL),
(478, 'produk', '107', 'img/src/produk/', 'fas_1629020642_92c9f484ae3eef48ea68.jpeg', '2021-08-15 16:44:02', '2021-08-15 16:44:02', NULL),
(479, 'produk', '108', 'img/src/produk/', 'fas_1629020642_172266244bbb3415832e.jpeg', '2021-08-15 16:44:02', '2021-08-15 16:44:02', NULL),
(480, 'produk', '109', 'img/src/produk/', 'fas_1629020642_9c7c9518d0d93d2c04a5.jpeg', '2021-08-15 16:44:02', '2021-08-15 16:44:02', NULL),
(481, 'store', '78', 'img/src/store/', 'fas_1629021055_7d71a9b2e05ed147b826.png', '2021-08-15 16:50:56', '2021-08-15 16:50:56', NULL),
(482, 'produk', '110', 'img/src/produk/', 'fas_1629021056_c9922f4426a1d7a2f4ad.jpeg', '2021-08-15 16:50:56', '2021-08-15 16:50:56', NULL),
(483, 'produk', '111', 'img/src/produk/', 'fas_1629021056_97c1bb0aaf2d5d42b0ab.jpeg', '2021-08-15 16:50:56', '2021-08-15 16:50:56', NULL),
(484, 'store', '79', 'img/src/store/', 'fas_1629021720_9f622f2e6d1b94baf140.jpeg', '2021-08-15 17:02:00', '2021-08-15 17:02:00', NULL),
(485, 'produk', '112', 'img/src/produk/', 'fas_1629021720_984dad54a53924df3cfa.jpeg', '2021-08-15 17:02:00', '2021-08-15 17:02:00', NULL),
(486, 'produk', '113', 'img/src/produk/', 'fas_1629021720_75028aeab057113abcf0.jpeg', '2021-08-15 17:02:00', '2021-08-15 17:02:00', NULL),
(487, 'produk', '114', 'img/src/produk/', 'fas_1629021720_70f3753f6b1c71a0d388.jpeg', '2021-08-15 17:02:00', '2021-08-15 17:02:00', NULL),
(488, 'produk', '115', 'img/src/produk/', 'fas_1629021720_ad39b52e216bd54d7091.jpeg', '2021-08-15 17:02:00', '2021-08-15 17:02:00', NULL),
(489, 'produk', '116', 'img/src/produk/', 'fas_1629021720_605d09de1e4ffe922ff1.jpeg', '2021-08-15 17:02:00', '2021-08-15 17:02:00', NULL),
(490, 'store', '80', 'img/src/store/', 'fas_1629022036_1db921a35328215a711c.jpeg', '2021-08-15 17:07:16', '2021-08-15 17:07:16', NULL),
(491, 'produk', '117', 'img/src/produk/', 'fas_1629022036_464902e90fe1a8bc4a19.jpeg', '2021-08-15 17:07:16', '2021-08-15 17:07:16', NULL),
(492, 'store', '81', 'img/src/store/', 'fas_1629022814_89e4c2529c2c453d43c4.jpeg', '2021-08-15 17:20:14', '2021-08-15 17:20:14', NULL),
(493, 'produk', '118', 'img/src/produk/', 'fas_1629022814_0ec2f9983d4c9b5585a1.jpeg', '2021-08-15 17:20:14', '2021-08-15 17:20:14', NULL),
(494, 'produk', '119', 'img/src/produk/', 'fas_1629022814_d4bc6a08b52d6a342e17.jpeg', '2021-08-15 17:20:14', '2021-08-15 17:20:14', NULL),
(495, 'produk', '120', 'img/src/produk/', 'fas_1629022814_50a5569079cbadeda410.jpeg', '2021-08-15 17:20:14', '2021-08-15 17:20:14', NULL),
(496, 'produk', '121', 'img/src/produk/', 'fas_1629022814_aeefe06ccb208b8bc60e.jpeg', '2021-08-15 17:20:14', '2021-08-15 17:20:14', NULL),
(497, 'produk', '122', 'img/src/produk/', 'fas_1629022814_d712fc09db2caa735567.jpeg', '2021-08-15 17:20:14', '2021-08-15 17:20:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_request` varchar(255) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `nim` varchar(100) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `datebirth` date DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `ktm` text DEFAULT NULL,
  `image` varchar(255) DEFAULT 'user.jpg',
  `password_hash` varchar(255) NOT NULL,
  `reset_hash` varchar(255) DEFAULT NULL,
  `reset_at` datetime DEFAULT NULL,
  `reset_expires` datetime DEFAULT NULL,
  `activate_hash` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `email_request`, `username`, `fullname`, `nim`, `gender`, `datebirth`, `phone_number`, `ktm`, `image`, `password_hash`, `reset_hash`, `reset_at`, `reset_expires`, `activate_hash`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'testgamepitt@gmail.com', NULL, 'admin', 'Admin Absensi', '5556666777', 'male', '1996-03-01', '08457677667', 'ktm.jpg', 'low_1624170134_0961a3976f036ec76a04.png', '$argon2i$v=19$m=65536,t=4,p=1$bGZpWVI3UG5qUWRRUks0dA$+LtE1I6AIbw4YeeavBGmCCKbYxkRtiYRvJRPpXB18PQ', '475154c16fee5be1e4a4807f96241107', '2023-01-16 15:09:36', NULL, NULL, 1, '2020-02-25 11:04:23', '2023-01-16 15:09:36', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_groups`
--
ALTER TABLE `auth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_catagory`
--
ALTER TABLE `product_catagory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `store_users_id_foreign` (`users_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `product_catagory`
--
ALTER TABLE `product_catagory`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
